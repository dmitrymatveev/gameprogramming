/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author dmitry
 */
public class MathUtil {
    
    /**
     * 
     * @param value
     * @param total
     * @return 
     */
    public static float percent(int value, int total) {
        
        return (value * 100.0f) / total;
    }
}
