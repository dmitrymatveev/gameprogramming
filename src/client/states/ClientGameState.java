package client.states;

import client.main.ClientApp;
import client.content.world.WorldSynchronizer;
import client.ui.gui.GameScreen;
import com.jme3.network.Client;
import com.jme3.network.Message;
import common.message.SceneGraphUpdate;
import common.core.NetworkAppState;
import common.message.MessageRegister;
import common.ui.GuiActionListener;

/**
 * Displays scene graph state that is broadcasted by the server.
 * @author Dmitry Matveev
 */
public class ClientGameState extends NetworkAppState<ClientApp, Client> 
implements GuiActionListener {

    private GameScreen screen;
    public ClientGameState() {
        super(MessageRegister.GAME_STATE);
    }

    @Override
    public void onEnabled() {
        screen = this.getApp().getGuiFrame().enableScreen(GameScreen.class);
        screen.addGuiListener(this);
    }

    @Override
    public void onDisabled() {
        this.getApp().getRenderer().resetGLObjects();
        this.getApp().getRenderer().cleanup();
        this.getApp().getRenderer().clearBuffers(true, true, true);
        this.getApp().getRenderer().clearClipRect();
        screen.setEnabled(false);
    }

    @Override
    public void messageReceived(Client s, Message msg) {
        if (msg instanceof SceneGraphUpdate) {
            SceneGraphUpdate m = (SceneGraphUpdate) msg;
            WorldSynchronizer.sync(this.getApp(), m.content);
            this.getApp().getWorld().startSimulation();
        }
    }

    @Override
    public void onAction(String actionName) {
    }
}
