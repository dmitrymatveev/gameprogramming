package client.states;

import client.main.ClientApp;
import client.ui.gui.HelpScreen;
import client.ui.gui.JoinServerForm;
import client.ui.gui.StartScreen;
import com.jme3.network.Client;
import com.jme3.network.Message;
import common.core.NetworkAppState;
import common.ui.AppFrame;
import common.ui.GuiActionListener;

/**
 * Displays start screen and allows players to connect to the server.
 * @author Dmitry Matveev
 */
public class ClientStartState 
extends NetworkAppState<ClientApp, Client>  
implements GuiActionListener {
    
    StartScreen screen;
    JoinServerForm joinForm;
    HelpScreen helpScreen;

    public ClientStartState() {
        super("start");
    }

    @Override
    public void onEnabled() {
        if (this.getApp().getNetwork() != null) {
            this.getApp().stopNetworkService();
        }
        AppFrame guiFrame = this.getApp().getGuiFrame();
        guiFrame.setVisible(true);
        screen = guiFrame.enableScreen(StartScreen.class);
        screen.addGuiListener(this);
        joinForm = new JoinServerForm(screen);
        joinForm.addGuiListener(this);
        joinForm.setHostAddress("localhost");
        joinForm.setPortNumber(1111);
    }

    @Override
    public void onDisabled() {
    }

    @Override
    public void onAction(String actionName) {
        AppFrame guiFrame = this.getApp().getGuiFrame();
        if (actionName.equals(StartScreen.JOIN)) {
            joinForm.open();
        }
        if (actionName.equals(StartScreen.EXIT)) {
            guiFrame.stop();
            this.getApp().stopNetworkService();
            this.getApp().stop();
            this.getApp().destroy();
        }
        if (actionName.equals(JoinServerForm.CONNECT)) {
            String host = joinForm.getHostAddress();
            int port = joinForm.getPortNumber();
            if (host.isEmpty() || port < 0) {
                joinForm.setMessage("enter correct host address and port number");
            }
            else {
                this.getApp().startNetworkService(host, port);
                joinForm.close();
            }
        }
        if (actionName.equals(JoinServerForm.CLOSE)) {
            joinForm.close();
        }
        if (actionName.equals(StartScreen.HELP)){
            this.getApp().setState(ClientHelpState.class);
        }
        if (actionName.equals(StartScreen.ABOUT)){
            this.getApp().setState(ClientAboutState.class);
        }
    }

    @Override
    public void messageReceived(Client s, Message msg) {
        
    }
}
