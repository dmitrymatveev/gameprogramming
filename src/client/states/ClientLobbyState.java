package client.states;

import client.main.ClientApp;
import common.core.NetworkClient;
import client.ui.gui.LobbyScreen;
import com.jme3.network.Client;
import com.jme3.network.Message;
import common.message.StateChanged;
import common.message.StartGame;
import common.core.NetworkAppState;
import common.message.ClientNameUpdate;
import common.message.MessageRegister;
import common.ui.GuiActionListener;

/**
 * Shows a list of players and their info that are currently connected the the
 * server.
 * @author Dmitry Matveev
 */
public class ClientLobbyState
extends NetworkAppState<ClientApp, Client>
implements GuiActionListener {
    
    private LobbyScreen lobbyScreen;
    
    public ClientLobbyState() {
        super(MessageRegister.LOBBY_STATE);
    }
    
    public void startGame() {
        this.getApp().getNetwork().send(new StartGame());
    }

    @Override
    public void onEnabled() {
        ClientApp app = this.getApp();
        lobbyScreen = app.getGuiFrame().enableScreen(LobbyScreen.class);
        lobbyScreen.addGuiListener(this);
        lobbyScreen.updatePlayersInfo();
        app.getNetwork().send(new ClientNameUpdate(app.getClientName()));
    }

    @Override
    public void onDisabled() {
    }

    @Override
    public void messageReceived(Client s, Message msg) {
        if (msg instanceof ClientNameUpdate) {
            ClientNameUpdate m = (ClientNameUpdate) msg;
            this.getApp().setClientName(m.playerName);
            lobbyScreen.updatePlayersInfo();
        }
    }

    @Override
    public void onAction(String actionName) {
        if (LobbyScreen.START_GAME.equals(actionName)) {
            this.getApp().getNetwork().send(new StartGame());
        }
        if (LobbyScreen.MAIN_MENU.equals(actionName)) {
            this.getApp().setState(ClientStartState.class);
        }
    }
}
