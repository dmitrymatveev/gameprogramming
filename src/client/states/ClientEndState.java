package client.states;

import client.main.ClientApp;
import client.ui.gui.EndScreen;
import com.jme3.network.Client;
import com.jme3.network.Message;
import common.core.NetworkAppState;
import common.message.MessageRegister;

/**
 * Displays game results.
 * @author Dmitry Matveev
 */
public class ClientEndState
        extends NetworkAppState<ClientApp, Client> {

    public ClientEndState() {
        super(MessageRegister.END_STATE);
    }

    @Override
    public void onEnabled() {
        EndScreen screen = this.getApp().getGuiFrame().enableScreen(EndScreen.class);
        
    }

    @Override
    public void onDisabled() {
    }

    @Override
    public void messageReceived(Client s, Message msg) {
    }
}
