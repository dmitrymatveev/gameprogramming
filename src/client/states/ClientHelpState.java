/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.states;

import client.main.ClientApp;
import client.ui.gui.HelpScreen;
import com.jme3.network.Client;
import com.jme3.network.Message;
import common.core.NetworkAppState;
import common.message.MessageRegister;
import common.message.StartGame;
import common.ui.GuiActionListener;

/**
 *
 * @author Chris
 */
public class ClientHelpState extends NetworkAppState<ClientApp, Client> 
    implements GuiActionListener{

    private HelpScreen screen;
    public ClientHelpState() {
        super(MessageRegister.HELP_STATE);
    }

    @Override
    public void onEnabled() {
        this.getApp().getGuiFrame().setVisible(true);
        screen = this.getApp().getGuiFrame().enableScreen(HelpScreen.class);
        screen.addGuiListener(this);
        screen.helpInfo();
    }

    @Override
    public void onDisabled() {
        screen.setVisible(false);
        screen.setEnabled(false);
        this.getApp().getRenderer().cleanup();
    }

    @Override
    public void messageReceived(Client s, Message msg) {
        //do nothing
    }

    @Override
    public void onAction(String actionName) {
        if (HelpScreen.MAIN_MENU.equals(actionName)) {
            this.getApp().setState(ClientStartState.class);
        }
        if (HelpScreen.NEXT.equals(actionName)) {
            //this.getApp().setState(ClientStartState.class);
        }
    }
    
    
}
