package client.ui.hud;

import common.content.Planet;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.BillboardControl;
import com.jme3.scene.control.Control;
import common.core.AssetsRegister;

/**
 * HUD element that visualizes current state of the planet.
 * This controller is intended to be used with Planet objects.
 * @author dmitry
 */
public class PlanetUnitCounter extends AbstractControl {
    private Planet planet;
    private BitmapText text;

    private AssetsRegister am;
    
    public PlanetUnitCounter(AssetsRegister am) {
        this.am = am;
    }
    
    private void createOverlay() {
        Node hud = new Node();
        BitmapFont font = am.getFont("default");
        text = new BitmapText(font, false);
        text.setSize(1);
        text.setColor(planet.getOwner().getColor());
        text.setLocalTranslation(1.5f, 1f, 0f);
        text.setQueueBucket(RenderQueue.Bucket.Transparent);
        hud.attachChild(text);
        
        BillboardControl bb = new BillboardControl();
        bb.setAlignment(BillboardControl.Alignment.Camera);
        hud.addControl(bb);
        planet.attachChild(hud);
    }
    
    /**
     * Update color to match planet owner (which is a player) color.
     */
    public void updateColor() {
        //TODO this is a duplicate method that can be moved to PlanetController abstract class
        text.setColor(planet.getOwner().getColor());
    }

    @Override
    protected void controlUpdate(float tpf) {
        String t = String.valueOf(planet.getUnitCount());
        text.setText(t);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSpatial(Spatial spatial) {
        //TODO prevent attaching this controler to nodes that is not a Planet
        //TODO duplicate functionality that can be moved to PlanetController abstract class
        super.setSpatial(spatial);
        planet = (Planet) spatial;
        this.createOverlay();
    }
}
