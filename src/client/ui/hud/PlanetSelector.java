/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui.hud;

import common.content.Planet;
import com.jme3.material.Material;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.BillboardControl;
import com.jme3.scene.control.Control;
import common.core.AssetsRegister;

/**
 * HUD element intended to visually distinguish currently selected planet.
 * This controller is intended to be used with Planet objects.
 * @author Dmitry Matveev
 */
public class PlanetSelector extends AbstractControl {

    private Planet planet;
    private Node selectorNode;
    private AssetsRegister am;
    private Material mat;

    public PlanetSelector(AssetsRegister am) {
        this.am = am;
    }
    
    @Override
    public void setSpatial(Spatial spatial) {
        //TODO prevent attaching this controler to nodes that is not a Planet
        super.setSpatial(spatial);
        this.planet = (Planet) spatial;
        this.selectorNode = this.createOverlay();
    }

    @Override
    protected void controlUpdate(float tpf) {
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private Node createOverlay() {
        
        Node hud = new Node();
        mat = am.getMaterial(planet.getOwner().getColor());
        
        Spatial model = am.getModel("overlay");
        model.rotate(90f, 0f, 0f);
        model.setQueueBucket(RenderQueue.Bucket.Transparent);
        model.setMaterial(mat);
        hud.attachChild(model);

        BillboardControl bb = new BillboardControl();
        bb.setAlignment(BillboardControl.Alignment.Camera);
        hud.addControl(bb);
        
        return hud;
    }
    
    /**
     * Attach selector to its planet node.
     */
    public void show() {
        
        planet.attachChild(this.selectorNode);
    }
    
    /**
     * Detach selector from its planet node.
     */
    public void hide() {
        
        planet.detachChild(this.selectorNode);
    }
    
    /**
     * Update selector color to match planet owner (which is a player) color.
     */
    public void updateColor() {

        mat.setColor("Color", planet.getOwner().getColor());
    }
}
