/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui.hud;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;

/**
 *
 * @author Kenji
 */
public class Skybox extends Node{
    
    private AssetManager assetManager;
    
    public Skybox(AssetManager assetManager){
        this.assetManager = assetManager;
        init();
    }
    
    /**
     * Setup the sky
     */
   public void init(){
        Texture north = assetManager.loadTexture("Texture/North.png");
        Texture south = assetManager.loadTexture("Texture/South.png");
        Texture west = assetManager.loadTexture("Texture/West.png");
        Texture east = assetManager.loadTexture("Texture/East.png");
        Texture up =    assetManager.loadTexture("Texture/Up.png");
        Texture down = assetManager.loadTexture("Texture/Down.png");
        attachChild(SkyFactory.createSky(assetManager, west, east, north, south, up, down));
} 
}

