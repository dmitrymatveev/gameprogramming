package client.ui.gui;

import common.ui.AbstractScreen;
import common.ui.GuiActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Dmitry Matveev
 */
public class JoinServerForm 
extends AbstractScreen
implements GuiActionListener {
    
    public static final String CONNECT = "Connect";
    public static final String CLOSE = "Close";
    
    private StartScreen holder;
    private JTextField hostAddress;
    private JTextField portNum;
    private JTextArea message;

    public JoinServerForm(StartScreen host) {
        this.setLayout(new BorderLayout());
        this.setSize(300, 350);
        this.holder = host;
        this.create();
    }
    
    private JButton createButton(final String name) {
        JButton btn = new JButton(name);
        btn.setPreferredSize(new Dimension(125, 33));
        
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                onAction(name);
            }
        });
        return btn;
    }

    private void create() {
        hostAddress = new JTextField(15);
        portNum = new JTextField(15);
        message = new JTextArea();
        message.setBackground( new Color(0, 0, 0, 1) );
        message.setEditable(false);
        
        JPanel cont = new JPanel();
        cont.add(new JLabel("Host: "));
        cont.add(hostAddress);
        cont.add(new JLabel("Port: "));
        cont.add(portNum);
        cont.add(this.createButton(CONNECT));
        cont.add(this.createButton(CLOSE));
        
        add(cont, BorderLayout.NORTH);
        add(message, BorderLayout.SOUTH);
        setVisible(false);
    }
    
    public void setMessage(String str) {
        this.message.setText(str);
    }
    
    public void setHostAddress(String txt) {
        this.hostAddress.setText(txt);
    }
    
    public String getHostAddress() {
        return this.hostAddress.getText().trim();
    }
    
    public void setPortNumber(int port) {
        this.portNum.setText(port+"");
    }
    
    public int getPortNumber() {
        int res = 0;
        try {
            String port = this.portNum.getText().trim();
            if (port.length() > 3 && port.length() < 6) {
                res = Integer.parseInt(this.portNum.getText().trim());
            }
        } catch (NumberFormatException e) {
            res = -1;
        }
        return res;
    }
    
    public void open() {
        this.holder.getCenterPanel().add(this);
        setVisible(true);
        this.holder.revalidate();
    }
    
    public void close() {
        this.holder.getCenterPanel().remove(this);
        setVisible(false);
        this.holder.revalidate();
        this.holder.repaint();
    }
}
