package client.ui.gui;

import common.ui.AbstractScreen;
import common.ui.GuiActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * Creates main screen.
 * @author Brijesh Travendi & Dmitry Matveev
 */
public class StartScreen 
extends AbstractScreen 
implements GuiActionListener {
    
    public static final String JOIN     = "Join";
    public static final String SETTINGS = "Settings";
    public static final String HELP     = "Help";
    public static final String EXIT     = "Exit";
    public static final String ABOUT    = "About";
    
    public JPanel logo;
    public JPanel center;
    public JPanel buttons;

    public StartScreen() {
        setLayout(new BorderLayout());
        //setBackground(Color.GRAY);
        this.create();
    }
    
    private JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4, 0));
        buttonPanel.setOpaque(!isOpaque());
        
        Dimension d = new Dimension(125, 33);
        buttonPanel.add(GuiFactory.createButton(JOIN, d, this));
        buttonPanel.add(GuiFactory.createButton(SETTINGS, d, this)); 
        buttonPanel.add(GuiFactory.createButton(HELP, d, this));
        buttonPanel.add(GuiFactory.createButton(ABOUT, d, this));
        buttonPanel.add(GuiFactory.createButton(EXIT, d, this));
        return buttonPanel;
    }
    
    private JPanel createLeftPanel() {
        JPanel leftPanel= new JPanel();
        leftPanel.setPreferredSize(new Dimension(200, 300));
        leftPanel.setOpaque(!isOpaque());
        return leftPanel;
    }
    
    private JPanel createButtonsPanel() {
        JPanel rightPanel= new JPanel();                
        rightPanel.setLayout(new BorderLayout());
        rightPanel.setPreferredSize(new Dimension(600, 300));
        rightPanel.setOpaque(false);
        return rightPanel;
    }
    
    private JPanel createLogoPanel() {
        JPanel logoPanel = new JPanel();
        logoPanel.setOpaque(!isOpaque());
        return logoPanel;
    }
    
    public JPanel getCenterPanel() {
        return this.center;
    }
    
    private void create() {
        logo = createLogoPanel();
        center = createLeftPanel();
        buttons = createButtonsPanel();
        
        buttons.add(createButtonPanel(),   BorderLayout.SOUTH);
        
        add(logo,      BorderLayout.PAGE_START);
        add(center,    BorderLayout.CENTER);
        add(buttons,   BorderLayout.SOUTH);
    }
}
