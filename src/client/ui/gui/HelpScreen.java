/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui.gui;

import common.ui.AbstractScreen;
import common.ui.GuiActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Chris
 */
public class HelpScreen extends AbstractScreen 
    implements GuiActionListener{
    
    public static final String NEXT = "Next";
    public static final String MAIN_MENU = "Main Menu";
    
    private JPanel helpPanel;
    private JPanel buttons;

    public HelpScreen() {
        this.setLayout(new BorderLayout());
        this.setVisible(true);
        
        this.helpPanel = new JPanel();
        this.helpPanel.setLayout(new BoxLayout(helpPanel, BoxLayout.Y_AXIS));
        JPanel playersInfoHolder = new JPanel();
        playersInfoHolder.add(this.helpPanel);
        
        this.add(playersInfoHolder, BorderLayout.CENTER);
        this.add(this.createButtonsPanel(), BorderLayout.SOUTH);
    }
    
    private JPanel createButtonsPanel() {
        this.buttons = new JPanel();
        Dimension d = new Dimension(125, 33);
        buttons.add(GuiFactory.createButton(NEXT, d, this));
        buttons.add(GuiFactory.createButton(MAIN_MENU, d, this));
        return this.buttons;
    }
    
    private JPanel createPanel(String msg) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(msg));
        return panel;
    }
    
    public void helpInfo() {
        
        String info = "";
        info += "<HTML>The goal of the game is to <br>"
                + "test </HTML>";
        this.add(createPanel(info));
        this.revalidate();
    }
}
