package client.ui.gui;

import common.ui.GuiActionListener;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Provides common GUI builder methods.
 * @author Dmitry Matveev
 */
public class GuiFactory {
    
    /**
     * Creates a new button.
     * @param n name
     * @param d size
     * @param l action listener
     * @return button instance
     */
    public static JPanel createButton(final String n, Dimension d, final GuiActionListener l) {
        JButton btn = new JButton(n);
        btn.setPreferredSize(new Dimension(d.width, d.height));
        JPanel panel = new JPanel();
        panel.add(btn);
        panel.setOpaque(false);
        
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                l.onAction(n);
            }
        });
        return panel;
    }
}
