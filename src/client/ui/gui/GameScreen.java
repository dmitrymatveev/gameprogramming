package client.ui.gui;

import client.main.ClientApp;
import common.ui.AbstractScreen;
import com.jme3.system.JmeCanvasContext;
import java.awt.Canvas;
import java.awt.Dimension;

/**
 *
 * @author Dmitry Matveev
 */
public class GameScreen 
extends AbstractScreen {
    
    private ClientApp app;
    private Canvas canvas;
    
    public GameScreen(ClientApp app) {
        this.app = app;
        canvas = ((JmeCanvasContext) app.getContext()).getCanvas();
        canvas.setPreferredSize(new Dimension(640, 480));
        this.add(canvas);
    }
}
