/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui.gui;

import common.ui.AbstractScreen;
import common.ui.GuiActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Chris
 */
public class AboutScreen extends AbstractScreen 
    implements GuiActionListener{
    
    public static final String NEXT = "Next";
    public static final String MAIN_MENU = "Main Menu";
    
    private JPanel helpPanel;
    private JPanel buttons;

    public AboutScreen() {
        this.setLayout(new BorderLayout());
        this.setVisible(true);
        
        this.helpPanel = new JPanel();
        this.helpPanel.setLayout(new BoxLayout(helpPanel, BoxLayout.Y_AXIS));
        JPanel gameInfoHolder = new JPanel();
        gameInfoHolder.add(this.helpPanel);
        
        this.add(gameInfoHolder, BorderLayout.CENTER);
        this.add(this.createButtonsPanel(), BorderLayout.SOUTH);
    }
    
    private JPanel createButtonsPanel() {
        this.buttons = new JPanel();
        Dimension d = new Dimension(125, 33);
        buttons.add(GuiFactory.createButton(NEXT, d, this));
        buttons.add(GuiFactory.createButton(MAIN_MENU, d, this));
        return this.buttons;
    }
    
    private JPanel createPanel(String msg) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(msg));
        return panel;
    }
    
    public void aboutInfo() {
        String info = "";
        info = "<HTML>This is a 3D online real-time strategy game. <br>"
                + "During a single game instance that lasts approximately 5-10 minutes, <br>"
                + "all participating players are required to acquire <br>"
                + "more planets and units to conquer any other neutral or opposing planets.</HTML>";
        this.add(createPanel(info));
        this.revalidate();
    }
}
