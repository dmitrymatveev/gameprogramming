package client.ui.gui;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Rectangle;
import common.ui.AbstractScreen;
import common.ui.GuiActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Brijesh Travendi & Dmitry Matveev
 */
public class LobbyScreen 
extends AbstractScreen 
implements GuiActionListener {
    
    public static final String START_GAME = "Start Game";
    public static final String MAIN_MENU = "Main Menu";
    
    private JPanel playersInfo;
    private JPanel buttons;

    public LobbyScreen() {
        this.setLayout(new BorderLayout());
        this.setVisible(true);
        
        this.playersInfo = new JPanel();
        this.playersInfo.setLayout(new BoxLayout(playersInfo, BoxLayout.Y_AXIS));
        JPanel playersInfoHolder = new JPanel();
        playersInfoHolder.add(this.playersInfo);
        
        this.add(playersInfoHolder, BorderLayout.CENTER);
        this.add(this.createButtonsPanel(), BorderLayout.SOUTH);
    }
    
    private JPanel createButtonsPanel() {
        this.buttons = new JPanel();
        Dimension d = new Dimension(125, 33);
        buttons.add(GuiFactory.createButton(START_GAME, d, this));
        buttons.add(GuiFactory.createButton(MAIN_MENU, d, this));
        return this.buttons;
    }
    
    private JPanel createPanel(String name, ColorRGBA color) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(name));
        JPanel box = new JPanel();
        box.setBackground(new Color(color.r, color.g, color.b));
        panel.add(box);
        return panel;
    }
    
    public void updatePlayersInfo() {
        this.add(createPanel("me", ColorRGBA.Black));
        this.revalidate();
//        this.repaint();
    }
}
