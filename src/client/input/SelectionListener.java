/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.input;

import common.content.Planet;

/**
 * The listener interface for receiving evens about scene graph interaction
 * generated by MouseSelection class.
 * @author Dmitry Matveev
 */
public interface SelectionListener {
    
    /**
     * Called when a user clicks on a planet.
     * @param planet selected planet
     */
    public void onPlanetSelected(Planet planet); 
    
    /**
     * Called when a user had selected a second planet in a row indicating
     * destination choice.
     * @param origin planet selected first
     * @param target planet selected last
     */
    public void onRouteSelected(Planet origin, Planet target);
}
