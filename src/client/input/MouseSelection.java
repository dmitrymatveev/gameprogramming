/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.input;

import client.ui.hud.PlanetSelector;
import common.content.Planet;
import com.jme3.collision.CollisionResults;
import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import java.util.LinkedList;

/**
 *
 * @author jerry
 * 
 * Modifications:
 * Dmitry       Cleaned up the code, introduced evalSelection method.
 */
public class MouseSelection extends LinkedList<SelectionListener> 
                            implements ActionListener {
    
    
    private InputManager input;
    private Camera cam;
    private Node rootNode;
    private Planet lastSelection;

    public MouseSelection(InputManager input, Camera cam, Node rootNode) {
        this.input = input;
        this.cam = cam;
        this.rootNode = rootNode;
        this.lastSelection = null;
        this.init();
    }
    
    private void init() {
        this.input.addMapping("pick", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        this.input.addListener(this, "pick");
        this.add(new PlanetSelectionToggle());
    }
    
    //determines which event to generate for SelectionListeners
    private void evalSelection(Node currentSelection) {
        if(currentSelection == null) {//reset selection
            for(SelectionListener l : this) {
                l.onPlanetSelected(null);
            }
            this.lastSelection = null;
        }
        else if(currentSelection instanceof Planet) {//check event conditions
            Planet selection = (Planet) currentSelection;
            //set to selection
            if(this.lastSelection == null) {
                for (SelectionListener l : this) {
                    l.onPlanetSelected(selection);
                }
                this.lastSelection = selection;
            }
            //reset selection
            else if(this.lastSelection == selection) {
                for (SelectionListener l : this) {
                    l.onPlanetSelected(null);
                }
                this.lastSelection = null;
            }
            //correct condition, call event then reset
            else if(this.lastSelection != selection) {
                for(SelectionListener l : this) {
                    l.onRouteSelected(this.lastSelection, selection);
                }
                for (SelectionListener l : this) {
                    l.onPlanetSelected(null);
                }
                this.lastSelection = null;
            }
        }
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (name.equals("pick") && !isPressed) {
            // Reset results list.
            CollisionResults results = new CollisionResults();
            // Convert screen click to 3d position
            Vector2f click2d = input.getCursorPosition();
            Vector3f click3d = cam.getWorldCoordinates(
                    new Vector2f(click2d.x, click2d.y), 0f).clone();
            Vector3f dir = cam.getWorldCoordinates(
                    new Vector2f(click2d.x, click2d.y), 1f)
                    .subtractLocal(click3d).normalizeLocal();
            // Aim the ray from the clicked spot forwards.
            Ray ray = new Ray(click3d, dir);
            // Collect intersections between ray and all nodes in results list.
            rootNode.collideWith(ray, results);
            // Use the results -- we rotate the selected geometry.
            if (results.size() > 0) {
                //TODO search collision results for the first planet
                // The closest result is the target that the player picked:
                Geometry target = results.getClosestCollision().getGeometry();
                Node parent = target.getParent().getParent().getParent();
                evalSelection(parent);
            }
            else {
                evalSelection(null);
            }
        }
    }
    
    private class PlanetSelectionToggle implements SelectionListener {
        @Override
        public void onPlanetSelected(Planet planet) {
            if(lastSelection != null) {
                lastSelection.getControl(PlanetSelector.class).hide();
            }
            
            if(planet != null) {
                planet.getControl(PlanetSelector.class).show();
            }
        }

        @Override
        public void onRouteSelected(Planet origin, Planet target) {
            //does nothing
        }
    }
}
