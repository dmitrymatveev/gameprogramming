package client.main;

import common.core.NetworkClient;
import common.core.AssetsRegister;
import common.core.NetworkApp;
import client.states.ClientLobbyState;
import client.states.ClientGameState;
import client.states.ClientEndState;
import client.states.ClientStartState;
import common.ui.AppFrame;
import client.ui.gui.StartScreen;
import client.ui.gui.EndScreen;
import client.ui.gui.GameScreen;
import client.ui.gui.LobbyScreen;
import client.content.world.ClientWorld;
import client.states.ClientAboutState;
import client.states.ClientHelpState;
import client.ui.gui.AboutScreen;
import client.ui.gui.HelpScreen;
import common.content.ColorMap;
import common.message.MessageRegister;
import common.message.StateChanged;
import com.jme3.math.ColorRGBA;
import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;

/**
 * Main client application, which is a client specific implementation of the
 * NetworkApp class. 
 * @author Dmitry Matveev
 */
public class ClientApp extends NetworkApp<NetworkClient, ClientWorld>
        implements MessageListener<Client> {

    private String clientName;

    public ClientApp(String name) {
        super(name);
    }
    
    /**
     * Returns client name.
     * @return client name.
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Sets client name.
     * @param clientName 
     */
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @Override
    public void registerAssets(AssetsRegister assets) {
        assets.registerModel("p0", "models/PlanetA.blend");
        assets.registerModel("p1", "models/PlanetB.blend");
        assets.registerModel("p2", "models/PlanetC.blend");
        assets.registerModel("p3", "models/PlanetD.blend");
        assets.registerModel("p4", "models/PlanetEarth.blend");
        assets.registerModel("unit", "models/small_ship.blend");
        assets.registerModel("overlay", "models/planet_overlay.blend");
        
        assets.registerTexture("clear", "Texture/clear.png");
        
        assets.registerFont("default", "Interface/Fonts/Default.fnt");
        
        for (ColorRGBA c : ColorMap.getAllColors()) {
            assets.registerMaterial(c, "Common/MatDefs/Misc/Unshaded.j3md");
        }
    }

    @Override
    public void registerScreens(AppFrame frame) {
        frame.registerScreen(new StartScreen());
        frame.registerScreen(new LobbyScreen());
        frame.registerScreen(new GameScreen(this));
        frame.registerScreen(new EndScreen());
        frame.registerScreen(new HelpScreen());
        frame.registerScreen(new AboutScreen());
    }

    @Override
    public ClientWorld createWorld() {
        return new ClientWorld(this);
    }

    @Override
    public NetworkClient createNetworkHandler() {
        return new NetworkClient();
    }

    @Override
    public void onStartService(NetworkClient network) {
        network.addMessageListener(this);
    }

    @Override
    public void onStopService(NetworkClient network) {
        network.addMessageListener(this);
    }

    @Override
    public void messageReceived(Client s, Message msg) {
        if (msg instanceof StateChanged) {
            StateChanged m = (StateChanged) msg;
            if (MessageRegister.LOBBY_STATE.equals(m.name)) {
                this.setState(ClientLobbyState.class);
            }
            if (MessageRegister.GAME_STATE.equals(m.name)) {
                this.setState(ClientGameState.class);
            }
            if (MessageRegister.END_STATE.equals(m.name)) {
                this.setState(ClientEndState.class);
            }
            if (MessageRegister.ABOUT_STATE.equals(m.name)) {
                this.setState(ClientAboutState.class);
            }
            if (MessageRegister.HELP_STATE.equals(m.name)) {
                this.setState(ClientHelpState.class);
            }
        }
    }

    @Override
    public void simpleInitApp() {
        super.simpleInitApp();
        System.out.println("client application");
        ClientBuilder.setupLights(this);
        ClientBuilder.setupCamera(this);
        this.getConsole().add(new ClientConsole());
        this.setState(ClientStartState.class);
    }
    
    public static void main(String[] args) {
        ClientBuilder builder = new ClientBuilder();
        builder.start();
    }
}
