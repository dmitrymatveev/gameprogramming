package client.main;

import client.main.ClientConsole.COMMAND;
import client.states.ClientLobbyState;
import common.console.ConsoleCommander;
import common.core.NetworkAppState;

/**
 * Console implementation that adds client specific commands.
 * @author Dmitry Matveev
 */
public class ClientConsole extends ConsoleCommander<ClientApp> {

    @Override
    public <E extends Enum> Class<E> getCommandsEnum() {
        return (Class<E>) COMMAND.class;
    }

    public enum COMMAND implements Command<ClientApp> {

        OPEN("open", "<open> [host] [hostPort] connect to server") {
            @Override
            public void processCommand(ClientApp app, String[] lines)
            throws ConsoleCommandException {
                
                if(lines.length >= 3) {
                    app.startNetworkService(lines[1], Integer.valueOf(lines[2]));
                }
            }
        },
        NAME("name", "<name> [player name] \tchange player name\n"
                + "<name>\t\t\tdisplay player name") {
            @Override
            public void processCommand(ClientApp app, String[] lines)
            throws ConsoleCommandException {
                if(lines.length >= 2) {
                    app.setClientName(lines[1]);
                }
                else {
                    System.out.println(app.getClientName());
                }
            }
        },
        START("start", "<start> \t\tstart game in lobby") {
            @Override
            public void processCommand(ClientApp app, String[] lines)
            throws ConsoleCommandException {
                NetworkAppState currentState = app.getCurrentState();
                if(currentState instanceof ClientLobbyState) {
                   ((ClientLobbyState) currentState).startGame();
                }
                else {
                    throw new ConsoleCommandException("Illegal client state");
                }
            }
        };
        
        private String description;
        private String name;
        
        private COMMAND(String name, String desc) {
            this.name = name;
            this.description = desc;
        }

        @Override
        public String getDescription() {
            return this.description;
        }

        @Override
        public String getName() {
            return this.name;
        }
    }
}
