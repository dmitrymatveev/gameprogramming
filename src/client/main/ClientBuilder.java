package client.main;

import client.states.ClientAboutState;
import client.states.ClientEndState;
import client.states.ClientLobbyState;
import client.states.ClientStartState;
import client.states.ClientGameState;
import client.states.ClientHelpState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.ChaseCamera;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.math.Vector3f;
import com.jme3.system.AppSettings;
import common.core.NetAppBuilder;
import common.core.NetworkApp;

/**
 * Client specific implementation of the network application builder.
 * Additionally to build strategy implementation this class is a ClientApp
 * visitor attempting to remove a clutter of setting up lights and camera.
 *
 * @author Dmitry Matveev
 */
public class ClientBuilder extends NetAppBuilder {

    /**
     * Sets application to use ChaseCamera and setups camera itself.
     * @param client 
     */
    protected static void setupCamera(ClientApp client) {
        client.getFlyByCamera().setEnabled(false);
        ChaseCamera chaseCam =
                new ChaseCamera(client.getCamera(), client.getRootNode(), client.getInputManager());
        chaseCam.setDefaultDistance(50f);
        chaseCam.setToggleRotationTrigger(new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        chaseCam.setRotationSensitivity(15f);
    }

    /**
     * Creates light sources.
     * @param client 
     */
    protected static void setupLights(ClientApp client) {
        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-0.1f, -1f, -1f).normalizeLocal());
        client.getRootNode().addLight(dl);
    }

    @Override
    public AppSettings createSettings() {
        AppSettings settings = new AppSettings(true);
        settings.setWidth(640);
        settings.setHeight(480);
        settings.setBitsPerPixel(24);
//        settings.setFullscreen(false);
        settings.setVSync(true);
        return settings;
    }

    @Override
    public NetworkApp createApplication() {
        ClientApp app = new ClientApp("Planet Run Client");
        app.setShowSettings(false);
        app.setDisplayFps(false);
        app.setDisplayStatView(false);
        return app;
    }

    @Override
    public void registerApplicationStates(AppStateManager mgr) {
        mgr.attach(new ClientStartState());
        mgr.attach(new ClientLobbyState());
        mgr.attach(new ClientGameState());
        mgr.attach(new ClientEndState());
        mgr.attach(new ClientHelpState());
        mgr.attach(new ClientAboutState());
    }
}
