package client.content.world;

import client.input.MouseSelection;
import client.main.ClientApp;
import client.content.control.HumanControl;
import common.content.Planet;
import common.content.Planet.PlanetDef;
import common.content.Player;
import common.content.Player.PlayerDef;
import common.content.Unit;
import common.content.Unit.UnitDef;
import common.message.SceneGraphUpdate.MapValue;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Updates client world depending on the data received from server.
 *
 * @author Dmitry Matveev
 */
public class WorldSynchronizer {
    //TODO optimize attribute access, store world and player node in static vars
    
    private static void syncDeadUnits(ClientWorld world, ArrayList<UnitDef> units) {
        for (UnitDef def : units) {
            Unit unit = world.getPlayersNode().getUnit(def.name);
            if (unit != null) {
                unit.getParent().detachChild(unit);
            }
        }
    }
    
    private static void syncUnits(ClientWorld world, 
            Player player, ArrayList<UnitDef> units) {
        
        for (UnitDef def : units) {
            Unit unit = world.getPlayersNode().getUnit(def.name);
            if (unit == null) {
                //TODO change unit constructor to accept Player as its parent
                world.createUnit(player, def);
            }
            else {
                unit.setLocalTranslation(def.loc);
            }
        }
    }
    
    private static void syncPlanets (ClientWorld world, 
            Player player, ArrayList<PlanetDef> planets) {
        
        for (PlanetDef pdef : planets) {
            Planet planet = world.getPlayersNode().getPlanet(pdef.name);
            if (planet == null) {
                world.createPlanet(player, pdef);
            }
            else {
                if (!planet.getOwner().getName().equals(player.getName())) {
                    planet.setOwner(player);
                }
                planet.setUserData(Planet.UNIT_COUNT, pdef.currentSize);
            }
        }
    }
    
    private static Player syncPlayer(ClientApp app, PlayerDef pdef) {
        Player player = app.getWorld().getPlayersNode().getPlayer(pdef.name);
        if(player == null) {
            player = app.getWorld().createPlayer(pdef.name, pdef.color);
            if(player.getName().equals(app.getClientName())) {
                
                MouseSelection ms = new MouseSelection(
                        app.getInputManager(), app.getCamera(), app.getRootNode());
                ms.add(new HumanControl(app, player));
            }
        }
        return player;
    }

    /**
     * Reads content data from provided map and updates objects in local scene
     * graph or creates those if content does not yet exists.
     * @param app holder of the scene graph
     * @param remote HashMap representation of the scene graph
     */
    public static void sync(ClientApp app,
            HashMap<PlayerDef, MapValue> remote) {
        ClientWorld world = app.getWorld();
        for (PlayerDef pdef : remote.keySet()) {
            Player p = syncPlayer(app, pdef);
            syncPlanets(world, p, remote.get(pdef).planets);
            syncUnits(world, p, remote.get(pdef).units);
            syncDeadUnits(world, remote.get(pdef).deadUnits);
        }
    }
}
