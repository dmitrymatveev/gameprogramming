package client.content.world;

import client.content.actor.ClientPlanet;
import client.content.actor.ClientPlayer;
import client.content.actor.ClientUnit;
import client.main.ClientApp;
import com.jme3.math.ColorRGBA;
import common.content.Planet.PlanetDef;
import common.content.Player;
import common.content.Player.PlayerDef;
import common.content.PlayersNode;
import common.content.Unit.UnitDef;
import common.content.World;

/**
 * Adds methods to create client based content implementations to the world node.
 * @author Dmitry Matveev
 */
public class ClientWorld extends World<ClientApp> {

    public ClientWorld(ClientApp application) {
        super(application);
    }
    @Override
    public Player createNeutralPlayerImplementation(PlayersNode node) {
        return new ClientPlayer(node, new PlayerDef("Neutral", ColorRGBA.Gray));
    }
    
    /**
     * Creates ClientPlayer.
     * @param name name of the player.
     * @param color assigned color to player.
     * @return client implementation of the player class
     */
    public Player createPlayer(String name, ColorRGBA color) {
        return new ClientPlayer(playersNode, new PlayerDef(name, color));
    }
    
    /**
     * Creates ClientPlanet
     * @param player initial owner of the planet
     * @param def initial planet state
     * @return client implementation of the planet class
     */
    public ClientPlanet createPlanet(Player player, PlanetDef def) {
        return new ClientPlanet(this.app.getAssetsRegister(), player, def);
    }
    
    /**
     * Creates ClientUnit.
     * @param player owner of the unit
     * @param def initial state of the unit
     * @return client implementation of the unit
     */
    public ClientUnit createUnit(Player player, UnitDef def) {
        return new ClientUnit(this.app.getAssetsRegister(), player, def);
    }
}
