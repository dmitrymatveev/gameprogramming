package client.content.actor;

import client.ui.hud.PlanetSelector;
import client.ui.hud.PlanetUnitCounter;
import common.content.Planet;
import common.content.Player;
import common.core.AssetsRegister;
import java.util.Random;

/**
 * Creates a Planet with added client specific visual elements.
 * @author Dmitry Matveev
 */
public class ClientPlanet extends Planet {

    /**
     * Creates new instance of the planet
     * @param am assets register
     * @param parent planet owner
     * @param pdef planet definition
     */
    public ClientPlanet(AssetsRegister am, Player parent, PlanetDef pdef) {
        super(parent, pdef);
        this.init(am);
    }

    private void init(AssetsRegister am) {
        Random gen = new Random();
        int type = gen.nextInt(5);
        String model = "p"+type;
        this.attachChild(am.getModel(model));
        this.addControl(new PlanetSelector(am));
        this.addControl(new PlanetUnitCounter(am));
    }

    @Override
    public void setOwner(Player t) {
        super.setOwner(t);
        this.getControl(PlanetUnitCounter.class).updateColor();
        this.getControl(PlanetSelector.class).updateColor();
    }
}
