package client.content.actor;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import common.content.Player;
import common.content.Unit;
import common.core.AssetsRegister;

/**
 * Creates a Unit with added client specific visual elements.
 * @author Dmitry Matveev
 */
public class ClientUnit extends Unit {

    public ClientUnit(AssetsRegister am, Player parent, UnitDef def) {
        super(parent, def);
        this.init(am);
    }
    
    private void init(AssetsRegister am){
        Material mat = am.getMaterial(ColorRGBA.White);
        this.setMaterial(mat);
        this.attachChild(am.getModel("unit"));
    }
}
