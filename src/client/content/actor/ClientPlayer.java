package client.content.actor;

import common.content.Player;
import common.content.PlayersNode;

/**
 * This class while not adding any functionality over its super class Player,
 * it exists to supplement application architecture.
 * @author Dmitry Matveev
 */
public class ClientPlayer extends Player {

    public ClientPlayer(PlayersNode parent, PlayerDef def) {
        super(parent, def);
    }
}
