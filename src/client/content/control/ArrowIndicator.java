/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.content.control;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import common.content.Owner;
import common.content.Player;
import common.content.Subordinate;
import common.core.AssetsRegister;

/**
 *
 * @author Kenji
 */
public class ArrowIndicator extends Node implements Subordinate<Owner>{
    
    
    public ArrowIndicator(AssetsRegister am){
        this.init(am);
        setControl();
    }
    
    public void init(AssetsRegister am){
        Spatial arrow = am.getModel("arrow");
        Material mat = am.getMaterial(ColorRGBA.White);
        arrow.setMaterial(mat);
        arrow.scale(3f);
        this.attachChild(arrow);
    }
    
    public void setControl(){
        this.addControl(new ArrowControl(Vector3f.UNIT_Y, 1));
    }
    
    public void removeArrow(){
        this.detachAllChildren();
    }
    
    @Override
    public Owner getOwner() {
        return (Player) this.getOwner();
    }

    @Override
    public void setOwner(Owner t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
