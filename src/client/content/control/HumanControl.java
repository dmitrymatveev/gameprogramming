/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.content.control;

import client.input.SelectionListener;
import client.main.ClientApp;
import common.core.NetworkClient;
import common.content.Planet;
import common.content.Player;
import common.message.PlanetRouteSelected;

/**
 * Provides behavior for the mouse selection events. 
 * This class is intended to use Player node that belongs to a human player.
 * @author Dmitry Matveev
 */
public class HumanControl implements SelectionListener {
    //TODO re-factor this class into a singleton
    Player humanPlayer;
    NetworkClient client;
    ClientApp app;

    public HumanControl(ClientApp app, Player humanPlayer) {
        this.app = app;
        this.client = app.getNetwork();
        this.humanPlayer = humanPlayer;
    }

    @Override
    public void onPlanetSelected(Planet planet) {
    }

    @Override
    public void onRouteSelected(Planet origin, Planet target) {
        PlanetRouteSelected m = new PlanetRouteSelected(
                humanPlayer.getName(), origin.getName(), target.getName());
        this.client.send(m);
        ArrowIndicator arrow = new ArrowIndicator(app.getAssetsRegister());
        arrow.setLocalTranslation(0f, 2f, 0f);
        target.attachChild(arrow);
    }
}
