/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.content.control;


import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import server.util.TimerCounter;
import server.util.TimerCounter.TimerListener;

/**
 *
 * @author Kenji
 */
public class ArrowControl extends AbstractControl implements TimerListener{
    
    private Vector3f axis;
    private float speed;
    private TimerCounter time;
    
    public ArrowControl(Vector3f axis, float speed){
        //timer
        this.time = new TimerCounter(2.0f);
        this.time.add(this);
        //rotation
        this.speed = speed;
        this.axis = new Vector3f(axis);
    }     

    @Override
    public void onTimer() {
        ArrowIndicator arrow = (ArrowIndicator) this.spatial;
        arrow.removeArrow();
    }
    
    public void setRotation(float tpf){
        float amount = speed * tpf;
        spatial.rotate(axis.x * amount, axis.y * amount, axis.z * amount);
    }

    @Override
    protected void controlUpdate(float tpf) {
        setRotation(tpf);
        this.time.update(tpf);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
