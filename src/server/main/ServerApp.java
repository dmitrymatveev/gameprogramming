package server.main;

import common.core.NetworkServer;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import server.states.ServerLobbyState;
import com.jme3.network.Server;
import common.core.AssetsRegister;
import common.message.StateChanged;
import common.core.NetworkApp;
import common.ui.AppFrame;
import javax.swing.JFrame;
import server.content.world.GameRules;
import server.content.world.ServerWorld;
import server.ui.gui.ServerScreen;

/**
 * Server specific implementation of the network application.
 * @author Dmitry Matveev
 */
public class ServerApp
extends NetworkApp<NetworkServer, ServerWorld>
implements ConnectionListener {
    
    public GameRules rules;
    public String output = "";

    public ServerApp(String name) {
        super(name);
    }

    @Override
    public NetworkServer createNetworkHandler() {
        return new NetworkServer();
    }

    /**
     * Returns game rules.
     * @return 
     */
    public GameRules getRules() {
        return rules;
    }

    @Override
    public void registerAssets(AssetsRegister assets) {
        assets.registerModel("unit", "models/small_ship.blend");
        assets.registerModel("planet", "models/PlanetA.blend");
    }

    @Override
    public void registerScreens(AppFrame frame) {
        frame.registerScreen(new ServerScreen());
    }

    @Override
    public ServerWorld createWorld() {
        return new ServerWorld(this);
    }

    @Override
    public void simpleInitApp() {
        super.simpleInitApp();
        System.out.println("server application");
//        this.flyCam.setEnabled(false);
        this.getConsole().add(new ServerConsole());
        this.setState(ServerLobbyState.class);
    }

    @Override
    public void onStartService(NetworkServer network) {
        network.addConnectionListener(this);
        this.rules = new GameRules(this.getWorld().getPlayersNode());
        this.setState(ServerLobbyState.class);
    }

    @Override
    public void onStopService(NetworkServer network) {
        network.removeConnectionListener(this);
    }

    @Override
    public void connectionAdded(Server server, HostedConnection hc) {
        System.out.println("client connected");
        output += "Client connected \n";
        hc.send(new StateChanged(this.getCurrentState().getStateName()));
    }

    @Override
    public void connectionRemoved(Server server, HostedConnection hc) {
        System.out.println("client disconnected");
        output += "Client disconnected \n";
    }
    
    public static void main(String[] args) {
        ServerBuilder app = new ServerBuilder();
        app.start();
    }
    
    public String getOutput(){
        
        return output;
    }
}
