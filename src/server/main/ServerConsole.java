package server.main;

import common.console.ConsoleCommander;

/**
 * Provides server specific commands.s
 * @author Dmitry Matveev
 */
public class ServerConsole extends ConsoleCommander<ServerApp> {

    @Override
    public <E extends Enum> Class<E> getCommandsEnum() {
        return (Class<E>) COMMAND.class;
    }

    public enum COMMAND implements Command<ServerApp> {

        OPEN("open", "<open> [hostPort] \t create new server") {
            @Override
            public void processCommand(ServerApp app, String[] lines)
            throws ConsoleCommandException {
                if(lines.length >= 2) {
                    int lnth = lines[1].length();
                    if(lnth > 3 && lnth < 6) {
                        app.startNetworkService(lines[0], Integer.valueOf(lines[1]));
                    }
                    else {
                        System.out.println("illegal port number");
                    }
                }
            }
        };
        
        private String description;
        private String name;
        
        private COMMAND(String name, String desc) {
            this.name = name;
            this.description = desc;
        }

        @Override
        public String getDescription() {
            return this.description;
        }

        @Override
        public String getName() {
            return this.name;
        }
    }
}
