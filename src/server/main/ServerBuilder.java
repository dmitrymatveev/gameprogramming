package server.main;

import com.jme3.app.state.AppStateManager;
import com.jme3.system.AppSettings;
import common.core.NetAppBuilder;
import common.core.NetworkApp;
import server.states.ServerEndState;
import server.states.ServerGameState;
import server.states.ServerLobbyState;

/**
 * Server specific implementation of the application building strategy.
 * @author Dmitry Matveev
 */
public class ServerBuilder extends NetAppBuilder {

    @Override
    public AppSettings createSettings() {
        AppSettings settings = new AppSettings(true);
        settings.setWidth(640);
        settings.setHeight(480);
        settings.setBitsPerPixel(24);
        settings.setFullscreen(false);
        settings.setVSync(true);
        return settings;
    }

    @Override
    public NetworkApp createApplication() {
        ServerApp app = new ServerApp("Planet Run Server");
        app.setShowSettings(false);
        app.setDisplayFps(false);
        app.setDisplayStatView(false);
        return app;
    }

    @Override
    public void registerApplicationStates(AppStateManager mgr) {
        mgr.attach(new ServerLobbyState());
        mgr.attach(new ServerGameState());
        mgr.attach(new ServerEndState());
    }
}
