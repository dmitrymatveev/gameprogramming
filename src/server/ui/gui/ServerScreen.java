/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ui.gui;

import client.ui.gui.GuiFactory;
import common.ui.AbstractScreen;
import common.ui.GuiActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import server.main.ServerApp;

/**
 *
 * @author Jerry
 */
public class ServerScreen extends AbstractScreen 
        implements GuiActionListener{
    
    public static final String START_SERVER     = "Start";
    public static final String EXIT     = "Exit";
    public static final String RESTART = "Restart";
    
    public JPanel logo;
    public JPanel txtFieldPanel;
    public JPanel center;
    
    public JTextArea textArea;
    public JList playerList;
    
    public ServerScreen() {
        setLayout(new BorderLayout());
        this.create();
    }
    
    private JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(3, 0));
        buttonPanel.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
        buttonPanel.setOpaque(!isOpaque());
        
        Dimension d = new Dimension(125, 33);
        buttonPanel.add(GuiFactory.createButton(START_SERVER, d, this));
        buttonPanel.add(GuiFactory.createButton(RESTART, d, this));
        buttonPanel.add(GuiFactory.createButton(EXIT, d, this));
        
        return buttonPanel;
    }
    
    private JPanel createLogoPanel() {
        JPanel logoPanel = new JPanel();
        logoPanel.setOpaque(!isOpaque());
        return logoPanel;
    }
    
    public JPanel getCenterPanel() {
        return this.center;
    }
    
    private JPanel createTxtFieldPanel() {
        JPanel centerPanel = new JPanel(new BorderLayout());
//        textArea = new JTextArea();
//        JScrollPane scrollPaneTxtArea = new JScrollPane(textArea);
        centerPanel.setPreferredSize(new Dimension(200, 300));
//        centerPanel.add(scrollPaneTxtArea, BorderLayout.CENTER);
        return centerPanel;
    }
    
    private JPanel CenterPanel() {
        JPanel buttonsPanel= new JPanel();                
        buttonsPanel.setLayout(new BorderLayout());
        buttonsPanel.setPreferredSize(new Dimension(600, 300));
        buttonsPanel.setOpaque(!isOpaque());
        return buttonsPanel;
    }
    
    private void create() {
        logo = createLogoPanel();
        txtFieldPanel = createTxtFieldPanel();
        center =  CenterPanel();
        
        center.add(createButtonPanel(),   BorderLayout.SOUTH);
        
        add(logo,      BorderLayout.PAGE_START);
        add(txtFieldPanel,    BorderLayout.CENTER);
        add(center,   BorderLayout.SOUTH);
    }
    
    
    
}

    

