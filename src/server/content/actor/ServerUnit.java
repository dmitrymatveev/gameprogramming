package server.content.actor;

import common.content.Unit;
import common.core.AssetsRegister;

/**
 * Server specific implementation of a unit.
 * @author Dmitry Matveev
 */
public class ServerUnit extends Unit {

    /**
     * Same as Unit.
     * @param am
     * @param parent
     * @param def 
     */
    public ServerUnit(AssetsRegister am, ServerPlayer parent, UnitDef def) {
        super(parent, def);
        this.init(am);
    }
    
    private void init(AssetsRegister am) {
        this.attachChild(am.getModel("unit"));
    }
}
