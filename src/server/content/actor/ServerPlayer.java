package server.content.actor;

import common.content.Player;
import common.content.PlayersNode;
import common.content.Unit.UnitDef;
import java.util.ArrayList;
import server.content.control.PlanetPopulationControl;

/**
 * Base class for server specific implementation of the players.
 * Specifies entry points for implementing classes to specify planet population
 * controls variants.
 * @author Dmitry Matveev
 */
public abstract class ServerPlayer extends Player {

    public boolean connected;
    private ArrayList<UnitDef> deadUnits;
    
    /**
     * Creates new instance of the player.
     * @param parent player parent node
     * @param def initial player state
     */
    public ServerPlayer(PlayersNode parent, PlayerDef def) {
        super(parent, def);
        this.connected = false;
        this.deadUnits = new ArrayList<UnitDef>();
    }
    
    /**
     * ENtry point to specify player specific planet control.
     * @return planet control
     */
    public abstract PlanetPopulationControl createPlanetControl();

    /**
     * Returns true if this player is connected to server.
     * @return true if player connected and false otherwise
     */
    public boolean isConnected() {
        return connected;
    }
    
    /**
     * Sets player connected status.
     * @param connected 
     */
    public void setConnected(boolean connected) {
        this.connected= connected;
    }

    /**
     * Returns a list used to store references to dead unit definitions.
     * @return 
     */
    public ArrayList<UnitDef> getDeadUnits() {
        return deadUnits;
    }
    
    /**
     * Notification that a player needs to update planet controls.
     * @param planet 
     */
    public void planetAttached(ServerPlanet planet) {
        planet.removeControl(PlanetPopulationControl.class);
        planet.addControl(this.createPlanetControl());
    }
}
