package server.content.actor;

import common.content.Planet;
import common.content.Player;
import common.core.AssetsRegister;
import server.content.control.UnitSpawnControl;

/**
 * Server specific implementation of the planet adding functionality to
 * modify planet state.
 * @author Dmitry Matveev
 */
public class ServerPlanet extends Planet {

    private AssetsRegister am;
    
    /**
     * Creates new instance of the planet
     * @param am assets register
     * @param parent planet owner
     * @param pdef planet definition
     */
    public ServerPlanet(AssetsRegister am, ServerPlayer parent, PlanetDef pdef) {
        super(parent, pdef);
        this.am = am;
        this.init(am);
    }
    
    private void init(AssetsRegister am) {
        this.attachChild(am.getModel("planet"));
    }

    /**
     * Starts a task of sending number of units to the provided destination.
     * @param destination target planet
     */
    public void sendUnits(ServerPlanet destination) {
        int amount = this.getUnitCount() / 2;
        UnitSpawnControl uSpawn;
        uSpawn = new UnitSpawnControl(am, destination, this, amount);
        this.addControl(uSpawn);
    }
    
    /**
     * Increments number of units by one if maximum growth is not reached.
     */
    public void createUnit() {
        int count = this.getUserData(Planet.UNIT_COUNT);
        int max = this.getUserData(Planet.MAX_COUNT);
        if(count < max) {
            this.setUserData(Planet.UNIT_COUNT, ++count);
        }
    }    
    
    /**
     * Adds a unit to the planet unit count. Unit is added disregarding planet
     * max unit capacity.
     * @param num 
     */
    public void putUnit() {
        int count = this.getUserData(Planet.UNIT_COUNT);
        this.setUserData(Planet.UNIT_COUNT, ++count);
    }
    
    /**
     * Subtracts a unit from the planet if unit count is greater then zero.
     */
    public void pullUnit() {
        int count = this.getUserData(Planet.UNIT_COUNT);
        if(count > 0) {
            this.setUserData(Planet.UNIT_COUNT, --count);
        }
    }

    /**
     * Causes an update of player specific controls.
     * @param t 
     */
    @Override
    public void setOwner(Player t) {
        super.setOwner(t);
        ((ServerPlayer) t).planetAttached(this);
    }
}
