package server.content.world;

import com.jme3.math.ColorRGBA;
import com.jme3.scene.Spatial;
import common.content.ColorMap;
import common.content.Planet;
import common.content.Player;
import common.content.Player.PlayerDef;
import common.content.PlayersNode;
import common.content.World;
import java.util.List;
import server.content.actor.ServerPlayer;
import server.main.ServerApp;
import server.content.control.AIControl;
import server.content.control.PlanetPopulationControl;
import server.content.control.PlayerControl;

/**
 * Server specific implementation of the world.
 * @author Dmitry Matveev
 */
public class ServerWorld 
extends World<ServerApp> {

    /**
     * Creates new instance of the world.
     * @param application 
     */
    public ServerWorld(ServerApp application) {
        super(application);
    }

    @Override
    protected Player createNeutralPlayerImplementation(PlayersNode node) {
        return new ServerPlayer(node, new PlayerDef("Neutral", ColorRGBA.Gray)) {

            @Override
            public PlanetPopulationControl createPlanetControl() {
                return new PlanetPopulationControl(GameRules.NEUTRAL_PLANET_GROWS_RATE);
            }
        };
    }
    
    /**
     * Creates world planets.
     */
    public void createPlanets() {
        MapFactory pFactory = new MapFactory(5, (ServerPlayer) playersNode.getNeutralPlayer());
        pFactory.createPlanets(this.getApp().getAssetsRegister(), 10);
    }
    
    /**
     * Creates a player.
     * @param name player name
     * @param isAi whether this player is AI or human player
     */
    public void addPlayer(String name, boolean isAi) {
        PlayerDef def = new PlayerDef(name, ColorMap.getRandomColor(playersNode));
        ServerPlayer p = new ServerPlayer(playersNode, def) {

            @Override
            public PlanetPopulationControl createPlanetControl() {
                return new PlanetPopulationControl(GameRules.PLAYER_PLANET_GROWS_RATE);
            }
        };
        p.addControl(new PlayerControl(this.getApp().getRules()));
        if(isAi) {
            AIControl ai = new AIControl(playersNode);
            p.addControl(ai);
            p.setConnected(true);
        }
        System.out.println("added player "+p.getName());
    }
    
    /**
     * Creates Ai players to even out the number of player in the world.
     */
    public void createMissingPlayers() {
        List<Player> nonNeutralPlayers = this.playersNode.getNonNeutralPlayers();
        if(nonNeutralPlayers.size() % 2 != 0) {
            this.addPlayer("Ai", true);
        }
    }
    
    /**
     * Arrange planets between players.
     */
    public void assignPlanetOwners() {
        int i = 0;
        for(Spatial s : playersNode.getNonNeutralPlayers()) {
            Player p = (Player) s;
            Planet pl = playersNode.getNeutralPlayer().getPlanets().get(i++);
            pl.setOwner(p);
        }
    }
}
