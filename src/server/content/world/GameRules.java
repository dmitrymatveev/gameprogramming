/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.content.world;

import common.content.Player;
import common.content.PlayersNode;
import java.util.List;
import server.content.actor.ServerPlanet;
import server.content.actor.ServerPlayer;

/**
 * Provides game play variables and condition check routines.
 * @author Dmitry Matveev
 */
public class GameRules {
    
    private PlayersNode players;
    private boolean isEndGame;
    private static final int MIN_PLAYER_COUNT = 1;
    
    public static final float NEUTRAL_PLANET_GROWS_RATE = 2f;
    public static final float PLAYER_PLANET_GROWS_RATE = 1.0f;
    
    /**
     * Creates new instance of game rules.
     * @param players 
     */
    public GameRules(PlayersNode players) {
        this.players = players;
        this.isEndGame = false;
    }
    
    /**
     * Maximum allowed number of players.
     * @return 
     */
    public static int maxPlayers() {
        return 4;
    }

    /**
     * Returns true if end game condition is reached.
     * @return 
     */
    public boolean isEndGame() {
        return isEndGame;
    }
    
    /**
     * Causes game rules to re-evaluate end condition state.
     */
    public void checkEndGameCondition() {
        List<Player> nonNeutralPlayers = 
                players.getNonNeutralPlayers();
        int numPlayersWithPlanets = nonNeutralPlayers.size();
        for (Player p : nonNeutralPlayers) {
            if (p.getPlanets().size() < 1) {
                numPlayersWithPlanets--;
            }
        }
        if (numPlayersWithPlanets < 2) {
            this.isEndGame = true;
        }
    }
    
    /**
     * Returns true if player owns the planet which he is trying to send units from.
     * @param player player under question
     * @param origin planet being used to send units from
     * @return true if operation is allowed
     */
    public boolean isSendingUnitsAllowed(ServerPlayer player, ServerPlanet origin) {
        if (player == origin.getParent()) {
            return true;
        }
        return false;
    }
}
