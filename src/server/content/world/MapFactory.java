/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.content.world;

import com.jme3.math.Vector3f;
import common.content.Planet;
import common.content.Planet.PlanetDef;
import common.core.AssetsRegister;
import server.content.actor.ServerPlanet;
import server.content.actor.ServerPlayer;
import server.util.VecUtil;

/**
 * Creates game world planets.
 * @author Christophe
 * 
 * Modified:
 * Dmitry Matveev   Re-factored separation planet creation routine into
 *                  creating and condition check.
 */
public class MapFactory {
    //TODO this is a single use object that can be converted into a pure static class
    private ServerPlayer neut;
    private int numPlanets;
    
    public static final float MIN_DISTANCE = 5f;
    
    /**
     * Creates new instance of map factory
     * @param numPlanets total number of planets to create
     * @param parent 
     */
    public MapFactory(int numPlanets, ServerPlayer parent){
        
        neut = parent;
        this.numPlanets = numPlanets;
    }
    
    /**
     * @return true if planet is positioned at a minimum distance from all
     * other planets.
     */
    private boolean isCorrectPosition(Vector3f vect) {
        boolean res = true;
        for(Planet p : this.neut.getPlanets()) {
            Vector3f pLoc = p.getLocalTranslation();
            float dist = vect.distance(pLoc);
            if(dist >= MapFactory.MIN_DISTANCE) {
                res = true;
            }
            else {
                res = false;
                break;
            }
        }
        return res;
    }
    
    /**
     * Creates planets.
     * @param am assets register to use for planets
     * @param dimension distance from the center of the map
     */
    public void createPlanets(AssetsRegister am, int dimension){
        Vector3f v = new Vector3f();        
        for (int i = 0; i < numPlanets; i++) {
            boolean correctLocation = false;
            while(!correctLocation) {
                v = VecUtil.nextVector3f(-dimension, dimension);
                correctLocation = this.isCorrectPosition(v);
            }
            String name = "Planet " + i;
            PlanetDef def = new PlanetDef(name, 10, 1, v.clone());
            ServerPlanet planet = new ServerPlanet(
                    am,
                    neut, //all planets are initially generated as neutral
                    def);
            planet.setOwner(neut);
        }
    }
}
