package server.content.control;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import server.util.TimerCounter;
import server.util.TimerCounter.TimerListener;
import server.content.actor.ServerPlanet;
import server.util.MathUtil;

/**
 * Controls planet unit grows rate and update planet state.
 * @author Dmitry Matveev
 */
public class PlanetPopulationControl 
extends AbstractControl
implements TimerListener { 
    
    private final float BASE_INTERVAL;
    private TimerCounter timer;

    @SuppressWarnings("LeakingThisInConstructor")
    public PlanetPopulationControl(float baseGrowsRate) {
        this.BASE_INTERVAL = baseGrowsRate;
        this.timer = new TimerCounter(1.0f);
        this.timer.add(this);
    }
    
    private float calcNewInterval() {
        ServerPlanet p = (ServerPlanet) this.getSpatial();        
        float ratio = MathUtil.percentFloat(p.getUnitCount(), p.getUnitCount());
        return BASE_INTERVAL + (BASE_INTERVAL * ratio);
    }

    @Override
    public void onTimer() {
        ServerPlanet planet = (ServerPlanet) this.spatial;
        planet.createUnit();
    }

    @Override
    protected void controlUpdate(float tpf) {
        this.timer.setInterval(this.calcNewInterval());
        this.timer.update(tpf);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
