/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.content.control;

import common.content.Player;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import server.content.world.GameRules;

/**
 * Sends notifications to game rules object causing it to evaluate game state.
 * @author dmitry
 */
public class PlayerControl extends AbstractControl {
    //TODO move this functionality to unit move control to notify rules object on planet state changed
    private GameRules rules;

    public PlayerControl(GameRules rules) {
        this.rules = rules;
    }

    @Override
    protected void controlUpdate(float tpf) {
        Player p = (Player) this.getSpatial();
        if(p.getPlanets().size() < 1) {
            this.rules.checkEndGameCondition();
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
