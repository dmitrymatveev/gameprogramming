/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.content.control;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import common.content.Unit.UnitDef;
import common.core.AssetsRegister;
import server.content.actor.ServerPlanet;
import server.content.actor.ServerPlayer;
import server.content.actor.ServerUnit;

/**
 * Spawns units. Pulls units from origin planet and assigns them a control
 * to travel towards a destination planet. When unit counter reaches zero this
 * control detaches itself from its spatial.
 * 
 * @author Dmitry Matveev
 */
public class UnitSpawnControl extends AbstractControl {
    
    private static int ID_COUNTER = 0;
    
    private AssetsRegister am;
    private ServerPlanet target;
    private ServerPlanet origin;
    private int unitCounter;
    private float lastSpawnTimer;
    private int id;

    public UnitSpawnControl(AssetsRegister am, 
            ServerPlanet target, ServerPlanet origin, int unitCounter) {
        this.am = am;
        this.target = target;
        this.origin = origin;
        this.unitCounter = unitCounter;
        this.lastSpawnTimer = 0f;
        
        this.id = UnitSpawnControl.ID_COUNTER;
        UnitSpawnControl.ID_COUNTER++;
        if(UnitSpawnControl.ID_COUNTER >= Integer.MAX_VALUE) {
            UnitSpawnControl.ID_COUNTER = 0;
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        this.lastSpawnTimer += tpf;
        if(this.unitCounter > 0 && this.lastSpawnTimer > 0.5f) {
            this.lastSpawnTimer = 0.0f;
            origin.pullUnit();
            String name = origin.getName()+"-"+target.getName()+"-"+id+"-"+unitCounter;
            ServerUnit u = new ServerUnit(this.am,
                    (ServerPlayer) origin.getParent(), 
                    new UnitDef(name, origin.getLocalTranslation()));
            UnitMoveControl uc = new UnitMoveControl(origin, target, 2.0f);
            u.addControl(uc);
            this.unitCounter--;
        }
        else if(this.unitCounter == 0) {
            this.spatial.removeControl(this);
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        return this;
    }
}
