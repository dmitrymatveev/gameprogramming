package server.content.control;

import common.content.Player;
import com.jme3.bounding.BoundingSphere;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import common.content.Unit.UnitDef;
import server.content.actor.ServerPlanet;
import server.content.actor.ServerPlayer;

/**
 * Moves spatial object from point A to point B in a direct line.
 * @author Dmitry Matveev
 */
public class UnitMoveControl extends AbstractControl {

    private float duration;
    private ServerPlanet origin;
    private ServerPlanet destination;
    private float counter;
    private boolean done;
    
    private Player originOwner;

    /**
     * Creates new instance of move control.
     *
     * @param origin starting location of the motion
     * @param destination end location of the motion
     * @param speed velocity of the motion
     */
    public UnitMoveControl(ServerPlanet origin, ServerPlanet destination, float speed) {

        this.origin = origin;
        this.destination = destination;
        
        this.originOwner = this.origin.getOwner();

        this.duration = this.origin.getLocalTranslation().distance(
                this.destination.getLocalTranslation()) / speed;
    }

    private float getDistanceToDestination() {
        return this.spatial.getLocalTranslation().distance(
                this.destination.getLocalTranslation());
    }

    private boolean isDestinationReached() {
        boolean res = false;
        CollisionResults results = new CollisionResults();
        BoundingSphere g = this.destination.getPlanetBounds();
        this.spatial.collideWith(g, results);
        if (results.size() > 0) {
            res = true;
        }
        return res;
    }
    
    // remove unit from the scene and make record of a dead unit
    private void removeUnitFromSceneGraph() {
        ServerPlayer parent = (ServerPlayer) this.spatial.getParent();
        parent.getDeadUnits().add(new UnitDef(
                this.spatial.getName(), this.spatial.getLocalTranslation()));
        this.originOwner.detachChild(this.spatial);
    }

    // adjust unit counter at a collision planet
    private void adjustDestinationPlanetUnitCounter() {
        if (this.destination.getParent() != this.origin.getParent()) {
            int count = this.destination.getUnitCount();
            if (count > 0) {
                this.destination.pullUnit();
                this.destination.pullUnit();
            } else if (count == 0) {
                this.destination.setOwner(this.originOwner);
                this.destination.putUnit();
            }
        } else {
            this.destination.putUnit();
        }
    }

    /**
     * Translates position of the spatial by a fraction of the speed towards a
     * destination location of the move in a direct line.
     *
     * @param f
     */
    @Override
    protected void controlUpdate(float f) {
        Vector3f originLoc, destinationLoc;
        originLoc = origin.getLocalTranslation();
        destinationLoc = destination.getLocalTranslation();
        if (!this.done && this.getDistanceToDestination() > 0.02f) {
            this.counter += f / this.duration;
            Vector3f next = new Vector3f();
            next.interpolate(originLoc, destinationLoc, this.counter);
            this.spatial.setLocalTranslation(next);
        } else if (!this.done) {
            this.spatial.setLocalTranslation(destinationLoc);
            this.done = !this.done;
        }
        if (this.isDestinationReached()) {
            this.removeUnitFromSceneGraph();
            this.adjustDestinationPlanetUnitCounter();
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //does nothing
    }

    @Override
    public Control cloneForSpatial(Spatial sptl) {
        Control res = new UnitMoveControl(
                this.origin,
                this.destination,
                this.origin.getLocalTranslation().distance(
                this.destination.getLocalTranslation()) / this.duration);
        res.setSpatial(sptl);
        return res;
    }
}
