/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.content.control;

import common.content.Planet;
import common.content.Player;
import common.content.PlayersNode;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import server.util.TimerCounter;
import server.util.TimerCounter.TimerListener;
import java.util.List;
import java.util.Random;
import server.content.actor.ServerPlanet;
import server.util.MathUtil;

/**
 * Controls player node determining when to send units to other planets.
 * @author Dmitry Matveev
 */
public class AIControl extends AbstractControl implements TimerListener {

    private TimerCounter timer;
    private PlayersNode players;
    private float lastActionTime;

    /**
     * Creates new instance of AI control.
     * @param players Node containing player nodes.
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public AIControl(PlayersNode players) {
        this.players = players;
        this.lastActionTime = .0f;
        this.timer = new TimerCounter(1.0f);
        this.timer.add(this);
    }

    @Override
    public void onTimer() {
        aiAction();
    }

    /*
     * Find a planet to attack
     */
    private ServerPlanet findTargetPlanet() {
        Planet res = null;
        int selfId = this.players.getChildIndex(this.getSpatial());
        int targetPlayerId = selfId;
        Random rand = new Random();
        while (targetPlayerId == selfId) {
            targetPlayerId = rand.nextInt(this.players.getQuantity());
        }
        Player pl = (Player) this.players.getChild(targetPlayerId);
        List<Planet> planets = pl.getPlanets();
        int size = planets.size();
        if (size > 0) {
            res = planets.get(rand.nextInt(planets.size()));
        }
        return (ServerPlanet) res;
    }

    /*
     * Finds planet which best to pull units from.
     */
    private ServerPlanet findLaunchPlanet(Planet target) {
        Planet best = null;
        Player player = (Player) this.getSpatial();
        List<Planet> planets = player.getPlanets();
        if (planets.size() > 0) {
            best = planets.get(0);
            for (int i = 1; i < planets.size(); i++) {
                Planet p = planets.get(i);
                int count = p.getUnitCount();
                if (count > target.getUnitCount() && count > best.getUnitCount()) {
                    best = p;
                }
            }
        }

        return (ServerPlanet) best;
    }

    /*
     * Evaluate if it is time to pull units from own planet.
     */
    private boolean doAttack() {
        boolean res = false;
        Player pl = (Player) this.spatial;
        float percentage = MathUtil.percentInt(pl.getUnitCount(), pl.getPlanetSize());
        if (this.lastActionTime > 1.0f && percentage >= 50.0f) {
            this.lastActionTime = .0f;
            res = true;
        }
        return res;
    }

    private void aiAction() {
        ServerPlanet target = this.findTargetPlanet();
        if (target != null && this.doAttack()) {
            ServerPlanet origin = this.findLaunchPlanet(target);
            if (origin != null) {
                origin.sendUnits(target);
            }
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        this.lastActionTime += tpf;
        this.timer.update(tpf);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
