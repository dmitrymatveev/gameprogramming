package server.states;

import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import common.content.Player;
import common.content.PlayersNode;
import common.message.PlanetRouteSelected;
import common.message.StateChanged;
import common.message.SceneGraphUpdate;
import common.core.NetworkAppState;
import common.message.MessageRegister;
import server.content.actor.ServerPlanet;
import server.content.actor.ServerPlayer;
import server.main.ServerApp;
import server.util.TimerCounter;
import server.util.TimerCounter.TimerListener;

/**
 * Controls game execution and periodically updates clients with game state.
 * @author Dmitry Matveev
 */
public class ServerGameState
        extends NetworkAppState<ServerApp, HostedConnection>
        implements TimerListener, MessageListener<HostedConnection> {

    public TimerCounter tcounter;
    private PlayersNode playersNode;

    public ServerGameState() {
        super(MessageRegister.GAME_STATE);
    }

    public TimerCounter getTimerCounter() {
        return tcounter;
    }

    private boolean isAllPlayersConnected(PlayersNode playersNode) {
        boolean allConnected = false;
        for (Player p : playersNode.getNonNeutralPlayers()) {
            ServerPlayer player = (ServerPlayer) p;
            if (player.isConnected()) {
                allConnected = true;
            } else {
                allConnected = false;
                break;
            }
        }
        return allConnected;
    }

    private boolean setPlayerActive(String playerName) {
        boolean isSpectator = true;
        ServerPlayer player = (ServerPlayer) playersNode.getPlayer(playerName);
        if (player != null) {
            player.setConnected(true);
            isSpectator = false;
        }
        return !isSpectator;
    }

    private void triggerSendingUnits(PlanetRouteSelected msg) {
        ServerPlayer player = (ServerPlayer) playersNode.getPlayer(msg.player);
        ServerPlanet origin = (ServerPlanet) playersNode.getPlanet(msg.origin);
        ServerPlanet dest = (ServerPlanet) playersNode.getPlanet(msg.destination);
        if (player != null && origin != null & dest != null
                && this.getApp().getRules().isSendingUnitsAllowed(player, origin)) {
            origin.sendUnits(dest);
        }
    }

    @Override
    public void onTimer() {
        //TODO switch state to end game state
        if (this.getApp().getRules().isEndGame()) {
            this.getApp().getRootNode().detachChild(
                    this.getApp().getWorld().getPlayersNode());
            System.out.println("Game Over");
            this.getApp().setState(ServerEndState.class);
        }
    }

    @Override
    public void onEnabled() {
        this.playersNode = this.getApp().getWorld().getPlayersNode();
        this.tcounter = new TimerCounter(1.0f);
        this.tcounter.add(this);
    }

    @Override
    public void onDisabled() {
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        this.tcounter.update(tpf);
        this.playersNode.updateLogicalState(tpf);
        
        SceneGraphUpdate m =
                new SceneGraphUpdate(this.getApp().getWorld().getPlayersNode());
        this.getApp().getNetwork().broadcast(m);
    }

    @Override
    public void messageReceived(HostedConnection s, Message msg) {
        if (msg instanceof StateChanged) {
            StateChanged m = (StateChanged) msg;
            boolean start = this.setPlayerActive(m.name)
                    && this.isAllPlayersConnected(playersNode);
            if (start) {
                this.getApp().getWorld().startSimulation();
            }
        }
        if (msg instanceof PlanetRouteSelected) {
            this.triggerSendingUnits((PlanetRouteSelected) msg);
        }
    }
}
