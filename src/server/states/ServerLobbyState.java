package server.states;

import client.ui.gui.JoinServerForm;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import common.message.StateChanged;
import common.message.StartGame;
import common.core.NetworkAppState;
import common.message.ClientNameUpdate;
import common.message.MessageRegister;
import common.ui.GuiActionListener;
import java.util.ArrayList;
import java.util.Collection;
import server.main.ServerApp;
import server.content.world.GameRules;
import server.content.world.ServerWorld;
import server.ui.gui.JoinNetworkForm;
import server.ui.gui.ServerScreen;

/**
 * Controls creation of the new game instance.
 * @author Dmitry Matveev
 */
public class ServerLobbyState 
extends NetworkAppState<ServerApp, HostedConnection>  
implements GuiActionListener{

    private ServerScreen screen;
    private JoinNetworkForm joinForm;
    private String host;
    private int port;
    
    public ServerLobbyState() {
        super(MessageRegister.LOBBY_STATE);
    }

    private void onSyncPlayerName(HostedConnection hc, String name) {
        Collection<HostedConnection> clients =
                this.getApp().getNetwork().getConnections();
        if (clients.size() < GameRules.maxPlayers()) {
            String n = name;
            if (n == null || n.length() < 1) {
                n = "Player " + hc.getId();
                StateChanged m = new StateChanged(n);
                hc.send(m);
            }
            this.getApp().getWorld().addPlayer(n, false);
            this.getApp().getNetwork().broadcast(new ClientNameUpdate(n));
        }
    }
    
    private void onStartGameMessage() {
        ServerWorld world = this.getApp().getWorld();
        world.createPlanets();
        world.createMissingPlayers();
        world.assignPlanetOwners();
        this.getApp().setState(ServerGameState.class);
    }

    @Override
    public void onEnabled() {
        this.getApp().getGuiFrame().setVisible(true);
        screen = this.getApp().getGuiFrame().enableScreen(ServerScreen.class);
        screen.addGuiListener(this);
        joinForm = new JoinNetworkForm(screen);
        joinForm.addGuiListener(this);
        joinForm.setHostAddress("localhost");
        joinForm.setPortNumber(1111);
    }

    @Override
    public void onDisabled() {
    }

    @Override
    public void messageReceived(HostedConnection s, Message msg) {
        if (msg instanceof ClientNameUpdate) {
            ClientNameUpdate m = (ClientNameUpdate) msg;
            this.onSyncPlayerName(s, m.playerName);
        }
        
        if (msg instanceof StartGame) {
            this.onStartGameMessage();
        }
    }

    @Override
    public void onAction(String actionName) {
        if (actionName.equals(ServerScreen.START_SERVER)) {
           joinForm.open();
        }
        if (actionName.equals(ServerScreen.RESTART)) {
            restart();
            System.out.println("Server Restarted");
        }
        if (actionName.equals(ServerScreen.EXIT)) {
            this.getApp().stopNetworkService();
            this.getApp().stop();
            this.getApp().destroy();
        }
        ArrayList ports = new ArrayList();
        if(!ports.contains(port)){
            if (actionName.equals(JoinServerForm.CONNECT)) {
                host = joinForm.getHostAddress();
                port = joinForm.getPortNumber();
                if (host.isEmpty() || port < 0) {
                    joinForm.setMessage("enter correct host address and port number");
                    System.out.println("port already used");
                }
                else {
                    this.getApp().startNetworkService(host, port);
                    ports.add(port);
                    System.out.println(ports);
                    joinForm.close();
                }
            }
        }
        if (actionName.equals(JoinServerForm.CLOSE)) {
            joinForm.close();
        }
    }
    
    private void restart(){
        System.out.println("Server Restarted");
        
        this.getApp().getWorld().getPlayersNode().removeAll();
        this.getApp().getContext().restart();
        ServerWorld world = this.getApp().createWorld();
        world.createMissingPlayers();
        world.assignPlanetOwners();
        
    }
}
