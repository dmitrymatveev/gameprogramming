package server.states;

import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import common.core.NetworkAppState;
import common.message.MessageRegister;
import common.message.StateChanged;
import server.main.ServerApp;

/**
 * Determines and notifies clients about the game results.
 * @author Dmitry Matveev
 */
public class ServerEndState 
        extends NetworkAppState<ServerApp, HostedConnection> {

    public ServerEndState() {
        super(MessageRegister.END_STATE);
    }

    @Override
    public void onEnabled() {
    }

    @Override
    public void onDisabled() {
    }

    @Override
    public void messageReceived(HostedConnection s, Message msg) {
    }
}
