/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

import com.jme3.math.Vector3f;
import java.util.Random;

/**
 * Provides vector helper methods.
 * @author Dmitry Matveev
 */
public class VecUtil {
    
    /**
     * Returns a randomly generated vector within specified bounds.
     * @param min
     * @param max
     * @return vector
     */
    public static Vector3f nextVector3f(int min, int max) {
        Random rand = new Random();
        Vector3f v = new Vector3f();
        float[] xyz = new float[3];
        int low  = Math.min(min, max);
        int high = Math.max(min, max);
        for(int i = 0; i < xyz.length; i++) {
            xyz[i] = (float) (low + (Math.random()*( (high - low) + rand.nextFloat())));
        }
        return new Vector3f(xyz[0], xyz[1], xyz[2]);
    }
}
