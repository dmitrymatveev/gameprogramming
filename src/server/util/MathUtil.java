/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

/**
 * Provides math helper methods.
 * @author dmitry
 */
public class MathUtil {
    
    public static int percentInt(int value, int total) {
        if (value == 0 && total == 0) return 0;
        return (value * 100) / total;
    }
    
    public static float percentFloat(int value, int total) {
        if (value == 0 && total == 0) return 0;
        return (value * 1.0f) / total;
    }
}
