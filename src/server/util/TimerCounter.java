package server.util;

import server.util.TimerCounter.TimerListener;
import java.util.LinkedList;

/**
 * Keeps track of elapsed time and updates listeners at specified time intervals.
 * @author Dmitry Matveev
 */
public class TimerCounter extends LinkedList<TimerListener> {
    
    private float interval;
    private float counter;

    /**
     * Creates new instance of timer.
     * @param period 
     */
    public TimerCounter(float period) {
        this.interval = period;
        this.counter = .0f;
    }

    /**
     * Sets timer interval.
     * @param period 
     */
    public void setInterval(float period) {
        this.interval = period;
    }

    /**
     * Returns timer interval.
     * @return 
     */
    public float getInterval() {
        return interval;
    }
    
    /**
     * Causes this timer to update and notify listeners if needed.
     * @param tpf 
     */
    public void update(float tpf) {
        counter += tpf;        
        if(counter > interval) {
            counter -= interval;
            for(TimerListener e : this) e.onTimer();
        }
    }
    
    /**
     * Provides entry point for timer listeners to be notified about an
     * elapsed time interval.
     */
    public interface TimerListener {        
        public abstract void onTimer();
    }
}
