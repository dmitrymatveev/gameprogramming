package common.message;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import common.content.Planet;
import common.content.Planet.PlanetDef;
import common.content.Player;
import common.content.Player.PlayerDef;
import common.content.PlayersNode;
import common.content.Unit;
import common.content.Unit.UnitDef;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import server.content.actor.ServerPlayer;

/**
 * Stores a representation of a server scene graph converted into a map.
 * Server is broadcasting this message to notify all clients of its internal
 * state.
 * @author Dmitry Matveev
 */
@Serializable
public class SceneGraphUpdate extends AbstractMessage {
    
    public HashMap<PlayerDef, MapValue> content;
    
    /**
     * Stores player children as lists of object definitions.
     */
    @Serializable
    public static class MapValue {
        public ArrayList<PlanetDef> planets;
        public ArrayList<UnitDef> units;
        public ArrayList<UnitDef> deadUnits;

        /**
         * Creates new instance of map value.
         */
        public MapValue() {
            planets = new ArrayList<PlanetDef>();
            units = new ArrayList<UnitDef>();
            this.deadUnits = new ArrayList<UnitDef>();
        }

        /**
         * Creates new instance of map value.
         * @param planets list planet defs
         * @param units list of unit defs
         * @param deadUnits list of units due for removal
         */
        public MapValue(ArrayList<PlanetDef> planets, 
                ArrayList<UnitDef> units, ArrayList<UnitDef> deadUnits) {
            this.planets = planets;
            this.units = units;
            this.deadUnits = deadUnits;
        }
    }
    
    /**
     * Creates new instance of message.
     */
    public SceneGraphUpdate() {
        this.content = new HashMap<PlayerDef, MapValue>();
    }
    
    /**
     * Creates new instance of message and constructs its content based on data
     * found in provided players node.
     * @param players 
     */
    public SceneGraphUpdate(PlayersNode players) {
        this.content = new HashMap<PlayerDef, MapValue>();
        this.construct(players);
    }
    
    private PlayerDef player(Player player) {
        return new PlayerDef(player.getName(), player.getColor());
    }
    
    private ArrayList<PlanetDef> planets(Player player) {
        ArrayList<PlanetDef> planetsList = new ArrayList<PlanetDef>();
        PlanetDef planetdef;
        for (Planet p : player.getPlanets()) {
            planetdef = new PlanetDef(
                    p.getName(),
                    p.getPlanetSize(),
                    p.getUnitCount(),
                    p.getLocalTranslation());
            planetsList.add(planetdef);
        }
        return planetsList;
    }
    
    private ArrayList<UnitDef> units(Player player) {
        ArrayList<UnitDef> unitList = new ArrayList<UnitDef>();
        UnitDef unitDef;
        for(Unit u : player.getUnits()) {
            unitDef = new UnitDef(u.getName(), u.getLocalTranslation());
            unitList.add(unitDef);
        }
        return unitList;
    }
    
    private ArrayList<UnitDef> deadUnits(Player player) {
        ArrayList<UnitDef> units = new ArrayList<UnitDef>();
        ServerPlayer p = (ServerPlayer) player;
        ArrayList<UnitDef> deadUnits = p.getDeadUnits();
        units.addAll(deadUnits);
        deadUnits.clear();
        return units;
    }
    
    private void construct(PlayersNode players) {
        
        Player player = players.getNeutralPlayer();
        PlayerDef pdef = player(player);
        ArrayList<PlanetDef> planets = planets(player);
        ArrayList<UnitDef> units = units(player);
        ArrayList<UnitDef> deadUnits = deadUnits(player);
        content.put(pdef, new MapValue(planets, units, deadUnits));
        
        List<Player> nonNeutralPlayers = players.getNonNeutralPlayers();
        for (Player pl : nonNeutralPlayers) {
            pdef = player(pl);
            planets = planets(pl);
            units = units(pl);
            deadUnits = deadUnits(pl);
            content.put(pdef, new MapValue(planets, units, deadUnits));
        }
    }
}
