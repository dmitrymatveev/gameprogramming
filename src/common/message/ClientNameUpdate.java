package common.message;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Used to synchronize player name between server and client.
 * @author Dmitry Matveev
 */
@Serializable
public class ClientNameUpdate extends AbstractMessage {
    
    public String playerName;
    
    public ClientNameUpdate() {
        this.playerName = "";
    }
    
    public ClientNameUpdate(String name) {
        this.playerName = name;
    }
}
