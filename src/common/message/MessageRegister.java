package common.message;

import com.jme3.network.serializing.Serializer;
import common.content.Planet.PlanetDef;
import common.content.Player.PlayerDef;
import common.content.Unit.UnitDef;
import common.message.SceneGraphUpdate.MapValue;

/**
 * Provides a common method to register networking messages. Called by 
 * StateBasedApp implementations to register all messages.
 * 
 * @author Dmitry Matveev
 */
public class MessageRegister {
    
    public static final String LOBBY_STATE = "lobby";
    public static final String GAME_STATE = "game";
    public static final String END_STATE = "endgame";
    public static final String HELP_STATE = "help";
    public static final String ABOUT_STATE = "about";
    
    /**
     * Registers all message classes with a Serializer.
     */
    public static void register() {
        
        //messages
        Serializer.registerClass(ClientNameUpdate.class);
        Serializer.registerClass(PlanetRouteSelected.class);
        Serializer.registerClass(SceneGraphUpdate.class);
        Serializer.registerClass(StartGame.class);
        Serializer.registerClass(StateChanged.class);
        
        //objects
        Serializer.registerClass(MapValue.class);
        Serializer.registerClass(PlayerDef.class);
        Serializer.registerClass(PlanetDef.class);
        Serializer.registerClass(UnitDef.class);
    }
}
