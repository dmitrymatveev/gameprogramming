package common.message;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Used to notify network about application state changed event.
 * @author Dmitry Matveev
 */
@Serializable
public class StateChanged extends AbstractMessage {
    
    public String name;
    
    public StateChanged() {
        name = "";
    }
    
    /**
     * @param name player name
     */
    public StateChanged(String name) {
        this.name = name;
    }
}