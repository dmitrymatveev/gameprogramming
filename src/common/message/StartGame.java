package common.message;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Message indicating that a player wishes to start game.
 * @author Dmitry Matveev
 */
@Serializable
public class StartGame extends AbstractMessage {}
