package common.message;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Message indicating that a player is sending units from origin planet to
 * destination planet.
 * @author Dmitry Matveev
 */
@Serializable
public class PlanetRouteSelected extends AbstractMessage {
    
    public String player;
    public String origin;
    public String destination;
    
    public PlanetRouteSelected() {
        this.player = "";
        this.origin = "";
        this.destination = "";
    }
    
    /**
     * Creates new instance of the message.
     * @param player name of the player who invoked an event
     * @param start name of the origin planet
     * @param end name of the destination planet
     */
    public PlanetRouteSelected(String player, String start, String end) {
        this.player = player;
        this.origin = start;
        this.destination = end;
    }
}
