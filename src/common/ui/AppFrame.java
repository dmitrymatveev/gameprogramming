package common.ui;

import java.awt.HeadlessException;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Main application frame used to manage AbstractScreen objects.
 * @author Dmitry Matveev
 */
public final class AppFrame extends JFrame {

    private HashMap<Class<AbstractScreen>, AbstractScreen> panels;

    /**
     * Creates new instance of the application frame.
     * @throws HeadlessException 
     */
    public AppFrame(String name) throws HeadlessException {
        super(name);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(640, 480);

        this.panels = new HashMap<Class<AbstractScreen>, AbstractScreen>();
        AppFrame.setDefaultLookAndFeelDecorated(true);
    }

    /**
     * Registers new screen using its name a key.
     * Duplicate screen names are not allowed.
     * @param screen 
     */
    public void registerScreen(AbstractScreen screen) {
        panels.put((Class<AbstractScreen>) screen.getClass(), screen);
    }
    
    /**
     * Sets this frame to use provided screen class as its content pane and
     * returns an instance of screen object.
     * @param <E> type of the screen object to enable
     * @param screenClass previously registered screen class
     * @return instance of the screen class or null if it is not registered
     */
    public <E extends AbstractScreen> E enableScreen(Class<E> screenClass) {
        @SuppressWarnings("element-type-mismatch")
        E get = (E) panels.get(screenClass);
        if (get != null) {
            this.setContentPane(get);
            this.validate();
        }
        return (E) get;
    }
        

    public void stop() {
        this.setVisible(false);
        this.dispose();
    }
}
