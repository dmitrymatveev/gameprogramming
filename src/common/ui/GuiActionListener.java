package common.ui;

/**
 * Provides entry point for implementing classes to receive notifications
 * about GUI event.
 * Since messages arrive as strings, implementing classes need to be aware of
 * the GUI screen specific notification codes.
 * @author Dmitry Matveev
 */
public interface GuiActionListener {
    
    /**
     * Called when GUI even occurs.
     * @param actionName 
     */
    public void onAction(String actionName);
}
