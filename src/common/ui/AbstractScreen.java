package common.ui;

import java.util.LinkedList;
import javax.swing.JPanel;

/**
 * Base class for all screen implementations.
 * @author Dmitry Matveev
 */
public abstract class AbstractScreen extends JPanel 
implements GuiActionListener {
    
    private LinkedList<GuiActionListener> list;

    /**
     * Creates new instance of the screen with a given name.
     * @param name name of the screen
     */
    public AbstractScreen() {
        list = new LinkedList<GuiActionListener>();
    }
    
    public String getScreenName() {
        return null;
    }
    
    /**
     * Adds GUI events listener
     * @param l GUI listener
     * @return same as Collection.add
     */
    public boolean addGuiListener(GuiActionListener l) {
        return list.add(l);
    }
    
    /**
     * Removes GUI event listener.
     * @param l GUI listener
     * @return same as Collection.removes
     */
    public boolean removeGuiListener(GuiActionListener l) {
        return list.remove(l);
    }

    @Override
    public void onAction(String actionName) {
        for (GuiActionListener l : list) {
            l.onAction(actionName);
        }
    }
}
