/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.content;

import com.jme3.scene.Node;

/**
 * Base class for all nodes within a game scene graph.
 * @author Dmitry Matveev
 */
public class NodeProxy extends Node {

    /**
     * Creates new NodeProxy object and attaches it to a provided node as a
     * child.
     * @param parent
     * @param name 
     */
    public NodeProxy(Node parent, String name) {
        
        this.setName(name);
        parent.attachChild(this);
    }
}
