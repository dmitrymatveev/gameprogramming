/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.content;

/**
 * Provides methods specific to a parent object.
 * @author Dmitry Matveev
 */
public interface Owner {
    
    /**
     * Returns current unit count.
     * @return unit count
     */
    int getUnitCount();
    
    /**
     * Returns implementation specific size.
     * See implementing classes.
     * @return size
     */
    int getPlanetSize();
}
