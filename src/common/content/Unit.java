package common.content;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 * Base unit class.
 * @author Dmitry Matveev
 */
public class Unit extends NodeProxy implements Subordinate<Planet> {
    
    /**
     * Stores unit state attributes.
     */
    @Serializable
    public static class UnitDef {
        public String name;
        public Vector3f loc;

        /**
         * Creates new instance of unit definition.
         */
        public UnitDef() {
            this.name = "";
            this.loc = new Vector3f();
        }
        
        /**
         * Creates new instance of unit definition.
         * @param name initial unit name
         * @param loc initial unit location
         */
        public UnitDef(String name, Vector3f loc) {
            this.name = name;
            this.loc = loc;
        }
    }
    
    /**
     * Creates new instance of a unit.
     * @param parent unit owner
     * @param def initial unit state
     */
    public Unit(Player parent, UnitDef def){
        super(parent, def.name);
        this.setLocalTranslation(def.loc);
    }
    
    @Override
    public Planet getOwner() {
        return (Planet) this.getParent();   
    }

    @Override
    public void setOwner(Planet t) {
        
        Planet p = this.getOwner();
        if(p != t) {
            
            p.detachChild(this);
            t.attachChild(this);
        }
    }
}
