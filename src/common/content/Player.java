/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.content;

import com.jme3.math.ColorRGBA;
import com.jme3.network.serializing.Serializable;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.LinkedList;
import java.util.List;

/**
 * Base Player class.
 * @author Dmitry Matveev
 */
public class Player extends NodeProxy implements Owner {

    private ColorRGBA color;
    
    /**
     * Stores player attributes.
     */
    @Serializable
    public static class PlayerDef {
        public String name;
        public ColorRGBA color;

        public PlayerDef() {
            name = "";
            color = null;
        }

        /**
         * Creates new instance of player definition.
         * @param name
         * @param color
         */
        public PlayerDef(String name, ColorRGBA color) {
            this.name = name;
            this.color = color;
        }
    }
    
    /**
     * Creates new instance of player.
     * @param parent
     * @param def 
     */
    protected Player(PlayersNode parent, PlayerDef def) {
        super(parent, def.name);
        this.color = def.color;
    }

    /**
     * Returns player color.
     * @return color
     */
    public ColorRGBA getColor() {
        return color;
    }

    /**
     * Returns current a number of units stationed at all owned planets.
     * @return number of units on all planets.
     */
    @Override
    public int getUnitCount() {
        int res = 0;
        for(Planet planet : this.getPlanets()) {
            res += planet.getUnitCount();
        }
        return res;
    }

    /**
     * Returns a list of all owned planets.
     * @return planets list
     */
    public List<Planet> getPlanets() {
        List<Planet> list = new LinkedList<Planet>();
        for (Spatial sp : this.getChildren()) {
            if (sp instanceof Planet) {
                list.add((Planet) sp);
            }
        }
        return list;
    }

    /**
     * Returns number of units.
     * @return number of owned units.
     */
    public List<Unit> getUnits() {
        List<Unit> list = new LinkedList<Unit>();
        for(Spatial sp : this.getChildren()) {
            if(sp instanceof Unit) {
                list.add((Unit) sp);
            }
        }
        return list;
    }

    /**
     * Returns owned planets number.
     * @return number of owned planets.
     */
    @Override
    public int getPlanetSize() {
        List<Planet> list = this.getPlanets();
        int counter = 0;
        for(Planet pl : list) {
            counter += pl.getPlanetSize();
        }
        return counter;
    }
}
