package common.content;

import common.core.NetworkApp;

/**
 * Base world class. This class is a visitor for a NetworkApp that changes
 * scene graph tree.
 * @author Dmitry Matveev
 */
public abstract class World<T extends NetworkApp> {
    
    protected T app;
    protected PlayersNode playersNode;
    
    /**
     * Creates new world class
     * @param application 
     */
    public World(T application) {
        this.app = application;
        this.init();
        this.stopSimulation();
    }
    
    /**
     * Entry point for implementing classes to define its specific implementations
     * of neutral player.
     * @param node parent node for all players
     * @return instance of neutral player implementation
     */
    protected abstract Player createNeutralPlayerImplementation(PlayersNode node);

    private void init() {
        this.playersNode = new PlayersNode(app.getRootNode());
        Player neut = this.createNeutralPlayerImplementation(playersNode);
        this.playersNode.setNeutralPlayer(neut);
    }
    
    /**
     * Returns NetworkApp object that this world is visiting.
     * @return network application instance
     */
    public T getApp() {
        return app;
    }

    /**
     * Returns players node.
     * @return player node
     */
    public PlayersNode getPlayersNode() {
        return playersNode;
    }
    
    /**
     * Attaches players node to the root node of the application.
     */
    public final void startSimulation() {
        app.getRootNode().attachChild(this.playersNode);
    }
    
    /**
     * Detaches players node from the root node of the application.
     */
    public final void stopSimulation() {
        app.getRootNode().detachChild(this.playersNode);
    }
    
    /**
     * Returns true if players node is attached to the root player that denotes
     * that its contents are getting processed during the application update
     * routine.
     * @return 
     */
    public final boolean isSimulating() {
        boolean res = false;
        if (playersNode.getParent() == app.getRootNode()) {
            res = true;
        }
        return res;
    }
}
