/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.content;

import com.jme3.bounding.BoundingSphere;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 * Base planet class.
 * @author Dmitry Matveev & chris
 */
public class Planet extends NodeProxy implements Subordinate<Player>, Owner {

    public static final String UNIT_COUNT = "unitcount";
    public static final String MAX_COUNT = "maxcount";
    
    /**
     * Stores planet state attributes.
     */
    @Serializable
    public static class PlanetDef {
        public String name;
        public int size;
        public int currentSize;
        public Vector3f location;

        public PlanetDef() {
            name = "";
            size = 0;
            currentSize = 0;
            location = new Vector3f();
        }

        /**
         * Creates new planet definition.
         * @param name planet name
         * @param size maximum units capacity
         * @param curSize current units count
         * @param loc planet location
         */
        public PlanetDef(String name, int size, int curSize, Vector3f loc) {
            this.name = name;
            this.size = size;
            this.currentSize = curSize;
            this.location = loc;
        }
    }

    /**
     * Creates new planet.
     * @param parent initial owner of the planet
     * @param pdef initial planet state
     */
    public Planet(Player parent, PlanetDef pdef) {
        super(parent, pdef.name);
        this.setLocalTranslation(pdef.location);
        this.setUserData(MAX_COUNT, pdef.size);
        this.setUserData(UNIT_COUNT, pdef.currentSize);
    }
    
    /**
     * Returns planet collision bounds.
     * @return bounds
     */
    public BoundingSphere getPlanetBounds() {
        //TODO create bounds once per game
        BoundingSphere b = new BoundingSphere();
        b.setCenter(this.getLocalTranslation());
        b.setRadius(1f);
        return b;
    }

    /**
     * Override Method to get the Owner of the planet
     * @return the owner of the planet
     */
    @Override
    public Player getOwner() {
        return (Player) this.getParent();
    }

    /**
     * Override Method to get the unit count in the planet.
     * @return the number of unit in the planet
     */
    @Override
    public void setOwner(Player t) {
        Player p = this.getOwner();
        if (p != t) {
            p.detachChild(this);
            t.attachChild(this);
        }
    }
    
    /**
     * Returns amount of units currently stationed at a planet.
     * @return number of units
     */
    @Override
    public int getUnitCount() {
        int count = this.getUserData(Planet.UNIT_COUNT);
        //TODO fix. take into account units being pulled by spawn controll
        return count;
    }

    /**
     * Returns planet maximum unit capacity.
     * @return the size of the planet
     */
    @Override
    public int getPlanetSize() {
        return this.getUserData(Planet.MAX_COUNT);
    }
}
