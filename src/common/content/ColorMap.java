package common.content;

import com.jme3.math.ColorRGBA;
import java.util.List;
import java.util.Random;

/**
 * Provides access to all available colors in the game.
 * @author Dmitry Matveev
 */
public class ColorMap {
    
    private static final ColorRGBA[] COLOR = new ColorRGBA[] {
        ColorRGBA.Blue,
        ColorRGBA.Cyan,
        ColorRGBA.Green,
        ColorRGBA.Orange,
        ColorRGBA.Pink,
        ColorRGBA.Red,
        ColorRGBA.Yellow,
        ColorRGBA.White,
        ColorRGBA.Gray
    };
    
    /**
     * Returns all possible colors.
     * @return colors
     */
    public static ColorRGBA[] getAllColors() {
        return COLOR;
    }
    
    /**
     * Returns all color allowed to be assigned to a player.
     * @return player colors
     */
    public static ColorRGBA[] getAllowedColors() {
        ColorRGBA[] res = new ColorRGBA[COLOR.length - 1];
        System.arraycopy(COLOR, 0, res, 0, res.length);
        return res;
    }
    
    private static ColorRGBA randomColor() {
        Random rand = new Random();
        ColorRGBA[] allowedColors = getAllowedColors();
        return allowedColors[rand.nextInt(allowedColors.length)];
    }
    
    // random color excluding specified colors and already reserved colors
    private static ColorRGBA randomAllowedColor(PlayersNode pn, ColorRGBA ... color) {
        // TODO implement keeping track of already checked colors exit if all colours are taken
        ColorRGBA res = null;
        while(res == null) {
            res = randomColor();
            for(ColorRGBA c : color) {
                List<Player> pl = pn.getNonNeutralPlayers();
                for(Player p : pl) {
                    if(res == p.getColor()) {
                        res = null;
                        break;
                    }
                }
            }
        }
        return res;
    }
    
    /**
     * Returns random color that is not taken by any other player.
     * @param node containing all current players
     * @return un-used random color
     */
    public static ColorRGBA getRandomColor(PlayersNode pn) {
        ColorRGBA res = ColorMap.randomAllowedColor(pn, getAllowedColors());
        return res;
    }
}
