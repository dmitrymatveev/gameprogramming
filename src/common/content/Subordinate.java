/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.content;

/**
 * Provides methods specific to a child node.
 * @author Dmitry Matveev
 * @param <T> type of the owner this Subordinate is a child of.
 */
public interface Subordinate<T extends Owner> {
    
    /**
     * Returns owner.
     * @return owner
     */
    T getOwner();
    
    /**
     * Sets owner
     * @param t owner
     */
    void setOwner(T t);
}
