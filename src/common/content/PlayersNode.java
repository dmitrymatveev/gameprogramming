/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.content;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Stores player nodes and provides methods to get relative information about them.
 * @author Dmitry Matveev
 */
public class PlayersNode extends NodeProxy {
    
    private Player neut;

    /**
     * Creates new instance of a players node.
     * @param parent 
     */
    public PlayersNode(Node parent) {
        super(parent, "PlayersNode");
        //this.neut = neutral;//new Player(this, new PlayerDef("Neutral", ColorRGBA.Gray));
    }
    
    /**
     * Sets neutral player to a provided player object.
     * @param neutralPlayer 
     */
    public void setNeutralPlayer(Player neutralPlayer) {
        this.neut = neutralPlayer;
    }

    /**
     * Returns neutral player.
     * @return neutral player
     */
    public Player getNeutralPlayer() {
        return neut;
    }
    
    /**
     * Returns all planets within a scene graph.
     * @return all planets in the game
     */
    public List<Planet> getAllPlanets() {
        LinkedList<Planet> res = new LinkedList<Planet>();
        for(Spatial s : this.getChildren()) {
            Player p = (Player) s;
            res.addAll(p.getPlanets());
        }
        
        return res;
    }
    
    /**
     * Return all players.
     * @return all players
     */
    public List<Player> getAllPlayers() {
        LinkedList<Player> res = new LinkedList<Player>();
        res.add(this.getNeutralPlayer());
        res.addAll(this.getNonNeutralPlayers());
        return res;
    }
    
    /**
     * Returns all non neutral players.
     * @return non-neutral players.
     */
    public List<Player> getNonNeutralPlayers() {
        LinkedList<Player> res = new LinkedList<Player>();
        for(Spatial s : this.getChildren()) {
            if(s != this.neut) {
                res.add((Player) s);
            }
        }
        return res;
    }
    
    /**
     * Returns a player with a specified name.
     * @param name player name
     * @return player with a given name or null if no such player exists
     */
    public Player getPlayer(String name) {
        Player res = null;
        Iterator<Player> iterator = this.getAllPlayers().iterator();
        while (res == null && iterator.hasNext()) {
            Player p = iterator.next();
            if(p.getName().equals(name)) {
                res = p;
            }
        }
        return res;
    }
    
    /**
     * Returns a planet with a specified name.
     * @param name planet name
     * @return planet with a given name or null if no such planet exists
     */
    public Planet getPlanet(String name) {
        Planet res = null;
        Iterator<Planet> iterator = this.getAllPlanets().iterator();
        while (res == null && iterator.hasNext()) {
            Planet p = iterator.next();
            if (p.getName().equals(name)) {
                res = p;
            }
        }
        return res;
    }
    
    /**
     * Returns a unit with a specified name.
     * @param name unit name
     * @return unit with a given name or null if no such unit exists
     */
    public Unit getUnit(String name) {
        Unit res = null;
        Iterator<Player> iterator = this.getAllPlayers().iterator();
        while (res == null && iterator.hasNext()) {
            Player p = iterator.next();
            for (Unit u : p.getUnits()) {
                if (u.getName().equals(name)) {
                    res = u;
                }
            }
        }
        return res;
    }
    
    public void removeAll(){
        for (int i = 0; i < getAllPlanets().size(); i++) {
            this.getAllPlanets().remove(i);
        }
        for (int i = 0; i < getAllPlayers().size(); i++) {
            getAllPlayers().remove(i);
        }
    }
}
