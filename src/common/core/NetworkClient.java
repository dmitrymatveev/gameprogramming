package common.core;

import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.Network;
import java.io.IOException;

/**
 * Client-specific implementation of networking.
 * @author Dmitry Matveev
 */
public class NetworkClient extends AbstractNetwork<Client, Client> {

    @Override
    public Client createNetworkObject(String host, int port) {
        Client res = null;
        try {
            res = Network.connectToServer(host, port);
            System.out.println("client started");
        } catch (IOException ex) {
            System.out.println("connection refused");
        }
        return res;
    }

    @Override
    public void onStartService() {
        if (network != null) {
            network.addMessageListener(this);
            network.start();
        }
    }

    @Override
    public void onStopService() {
        if (network != null && network.isConnected()) {
            network.close();
            System.out.println("client stopped");
        }
    }
    
    /**
     * Same as Client.send
     * @param msg 
     */
    public void send(Message msg) {
        this.network.send(msg);
    }
}
