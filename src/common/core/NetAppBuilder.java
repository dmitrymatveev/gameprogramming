package common.core;

import com.jme3.app.state.AppStateManager;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;
import java.util.logging.Level;
import javax.swing.SwingUtilities;

/**
 * ClientApp builder strategy, provides entry points for set-up strategy of an 
 * application instance.
 * @author Dmitry Matveev
 */
public abstract class NetAppBuilder {
    
    public abstract AppSettings createSettings();
    public abstract NetworkApp createApplication();
    public abstract void registerApplicationStates(AppStateManager mgr);
    
    NetworkApp app;
    static {
        java.util.logging.Logger.getLogger("").setLevel(Level.WARNING);
    }
    
    private void startCanvas(NetworkApp app) {       
        this.app = app;
        app.createCanvas();
        JmeCanvasContext ctx = (JmeCanvasContext) app.getContext();
        ctx.setSystemListener(app);
        app.startCanvas();
    }
    
    /**
     * Initializes application and starts its execution.
     */
    public void start() {
        
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                AppSettings set = createSettings();
                NetworkApp app = createApplication();
                registerApplicationStates(app.getStateManager());
                app.setSettings(set);
                startCanvas(app);
            }
        });
        
//        java.awt.EventQueue.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//
//                AppSettings set = createSettings();
//                NetworkApp app = createApplication();
//                registerApplicationStates(app.getStateManager());
//                app.setSettings(set);
//                startCanvas(app);
//            }
//        });
    }
}
