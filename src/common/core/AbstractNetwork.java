package common.core;

import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageConnection;
import com.jme3.network.MessageListener;
import com.jme3.network.Server;
import common.message.StateChanged;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Base network representation class. Provides entry points for implementing
 * classes to provide client or server specific operations.
 * @author Dmitry Matveev
 * @param <N> type of the network object that can send and receive messages,
 * must be either Server or Client
 * @param <M> type of message listener object
 */
public abstract class AbstractNetwork<N extends Object, M extends MessageConnection>
implements MessageListener<M> {
    
    protected N network;
    
    private ArrayList<MessageListener<M>> messageListenersList;
    private ConcurrentLinkedQueue<MessageQueueItem> msgQueue;
    
    /**stores message details*/
    private class MessageQueueItem {
        public M messageConnection;
        public Message msg;

        public MessageQueueItem(M connection, Message msg) {
            this.messageConnection = connection;
            this.msg = msg;
        }
    }

    public AbstractNetwork() {
        this.msgQueue = new ConcurrentLinkedQueue<MessageQueueItem>();
        this.messageListenersList = new ArrayList<MessageListener<M>>();
    }
    
    /**
     * Create implementation specific network object of the N type.
     * @param host network address of the application
     * @param port network port of the application
     * @return instance of the network object
     */
    public abstract N createNetworkObject(String host, int port);
    
    /**
     * Start running network object here.
     * @param network 
     */
    public abstract void onStartService();
    
    /**
     * Stop network object here. 
     * @param network 
     */
    public abstract void onStopService();
    
    /**
     * Returns network object.
     * @return networking capable object
     */
    public N getNetwork() {
        return this.network;
    }
    
    /**
     * Helper method to send or broadcast a 'StateChanged' message depending on 
     * the network implementation. Used by NetworkAppState abstraction to
     * synchronize client and server state.
     * @param smg 
     */
    public void onStateChanged(StateChanged smg) {
        if (this.network instanceof Server) {
            Server s = (Server) network;
            s.broadcast(smg);
        }
        else if (this.network instanceof Client) {
            Client c = (Client) network;
            if (c.isConnected()) {
                c.send(smg);
            }
        }
    }
    
    /**
     * Same as Server or Client addMessageListener method
     * @param l listener
     */
    public void addMessageListener(MessageListener<M> l) {
        this.messageListenersList.add(l);
    }
    
    /**
     * Same as Server or Client removeMessageListener method
     * @param l listener
     */
    public void removeMessageListener(MessageListener<M> l) {
        this.messageListenersList.remove(l);
    }

    @Override
    public void messageReceived(M s, Message msg) {
        this.msgQueue.add(new MessageQueueItem(s, msg));
    }
    
    /**
     * Notifies all message listeners about received messages and clears
     * messages buffer.
     */
    public void processNetworkQueue() {
        Iterator<MessageQueueItem> iterator = this.msgQueue.iterator();
        while(iterator.hasNext()) {
            MessageQueueItem i = iterator.next();
            for (MessageListener<M> l : this.messageListenersList) {
                l.messageReceived(i.messageConnection, i.msg);
            }
            iterator.remove();
        }
    }
}
