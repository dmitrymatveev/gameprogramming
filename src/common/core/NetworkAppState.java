package common.core;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.network.MessageConnection;
import com.jme3.network.MessageListener;
import common.message.StateChanged;

/**
 * Base class for all application states.
 * @author Dmitry Matveev
 * @param <APP> type of application implementation
 * @param <MC> type of message listener object
 */
public abstract class NetworkAppState<APP extends NetworkApp, MC extends MessageConnection> 
extends AbstractAppState
implements MessageListener<MC> {
    
    private String name;
    private APP app;

    /**
     * Creates new instance of the application state.
     * @param name 
     */
    public NetworkAppState(String name) {
        this.name = name;
        this.setEnabled(false);
    }
    
    /**
     * Called when this state is enabled.
     */
    public abstract void onEnabled();
    
    /**
     * Called when this state is disabled.
     */
    public abstract void onDisabled();

    /**
     * Returns state name.
     * @return name
     */
    public String getStateName() {
        return name;
    }
    
    /**
     * Returns application object of this state.
     * @return application
     */
    public APP getApp() {
        return this.app;
    }

    @Override
    public final void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if(enabled && this.isInitialized()) {
            this.onEnabled();
            app.getNetwork().addMessageListener(this);
            app.getNetwork().onStateChanged(new StateChanged(name));
            System.out.println(this.getStateName()+ " state enabled");
        }
        else if (!enabled && this.isInitialized()) {
            if (app != null && app.getNetwork() != null) {
                app.getNetwork().removeMessageListener(this);
            }
            this.onDisabled();
        }
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (APP) app;
        System.out.println(name + " state initialized");
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        app.getNetwork().processNetworkQueue();
    }
}
