package common.core;

import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.Network;
import com.jme3.network.Server;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Server-specific implementation of networking.
 * @author Dmitry Matveev
 */
public class NetworkServer extends AbstractNetwork<Server, HostedConnection>
implements ConnectionListener {

    private ArrayList<ConnectionListener> connectionListenersList;

    public NetworkServer() {
        this.connectionListenersList = new ArrayList<ConnectionListener>();
    }
    
    @Override
    public Server createNetworkObject(String host, int port) {
        Server res = null;
        try {
            res = Network.createServer(port);
            System.out.println("server started");
        } catch (IOException ex) {
            System.out.println("server start failed");
        }
        return res;
    }

    @Override
    public void onStartService() {
        network.addConnectionListener(this);
        network.addMessageListener(this);
        network.start();
    }

    @Override
    public void onStopService() {
        if (network != null && network.isRunning()) {
            network.removeConnectionListener(this);
            network.removeMessageListener(this);
            network.close();
            System.out.println("server stopped");
        } else {
            System.out.println("server not running");
        }
    }
    
    /**
     * Same as Server.addConnectionListener
     * @param l listener
     */
    public void addConnectionListener(ConnectionListener l) {
        this.connectionListenersList.add(l);
    }
    
    /**
     * Same as Server.removeConnectionListener
     * @param l listener
     */
    public void removeConnectionListener(ConnectionListener l) {
        this.connectionListenersList.remove(l);
    }

    @Override
    public void connectionAdded(Server server, HostedConnection hc) {
        for (ConnectionListener l : this.connectionListenersList) {
            l.connectionAdded(server, hc);
        }
    }

    @Override
    public void connectionRemoved(Server server, HostedConnection hc) {
        for (ConnectionListener l : this.connectionListenersList) {
            l.connectionRemoved(server, hc);
        }
    }
    
    /**
     * Same as Server.getConnections
     * @return 
     */
    public Collection<HostedConnection> getConnections() {
        return this.network.getConnections();
    }
    
    /**
     * Same as Server.broadcast
     * @param msg 
     */
    public void broadcast(Message msg) {
        this.network.broadcast(msg);
    }
}
