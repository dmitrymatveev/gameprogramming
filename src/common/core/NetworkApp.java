package common.core;

import com.jme3.app.SimpleApplication;
import common.console.CommonConsole;
import common.console.Console;
import common.message.MessageRegister;
import common.content.World;
import common.ui.AppFrame;

/**
 * Base class for all network applications.
 * @author Dmitry Matveev
 * @param <N> type of AbstractNetwork implementation
 * @param <T> type of the world implementation this application is designed to work with.
 */
public abstract class NetworkApp<N extends AbstractNetwork, T extends World> 
extends SimpleApplication {

    private String appName;
    private AppFrame guiFrame;
    private AssetsRegister assets;
    private NetworkAppState currentState;
    private Class<? extends NetworkAppState> nextState;
    private Console input;
    private T world;
    private N network;

    public NetworkApp(String name) {
        this.appName = name;
    }
    
    /**
     * Register application GUI screens here.
     * @param frame main application frame
     */
    public abstract void registerScreens(AppFrame frame);
    //TODO delegate registering assets to NetworkAppState
    //TODO implement staged assets loading
    /**
     * Register all application assets here.
     * @param assets assets register instance
     */
    public abstract void registerAssets(AssetsRegister assets);
    
    /**
     * Create implementation specific network object of the N type.
     * @param host network address of the application
     * @param port network port of the application
     * @return instance of the network object
     */
    public abstract N createNetworkHandler();
    
    /**
     * Create implementation specific world object.
     * @return world
     */
    public abstract T createWorld();
    
    /**
     * Start running network object here.
     * @param network 
     */
    public abstract void onStartService(N network);
    
    /**
     * Stop network object here. 
     * @param network 
     */
    public abstract void onStopService(N network);
    
    /**
     * Returns current application state.
     * @return application state
     */
    public NetworkAppState getCurrentState() {
        return this.currentState;
    }

    /**
     * Returns application console handler.
     * @return console
     */
    public Console getConsole() {
        return input;
    }

    /**
     * Returns world.
     * @return world
     */
    public T getWorld() {
        return world;
    }

    /**
     * Returns network object.
     * @return networking capable object
     */
    public N getNetwork() {
        return network;
    }

    /**
     * Returns AssetsRegister object.
     * @return assets register
     */
    public AssetsRegister getAssetsRegister() {
        return assets;
    }

    /**
     * Returns GUI object.
     * @return user interface holder
     */
    public AppFrame getGuiFrame() {
        return guiFrame;
    }
    
    /**
     * Sets state of specified class to current for the application.
     * @param c state class to enable
     */
    public void setState(Class<? extends NetworkAppState> c) {
        this.nextState = c;
    }
    
    /**
     * Initializes all application modules.
     * @param host desired host address
     * @param port desired port number
     */
    public void startNetworkService(String host, int port) {
        MessageRegister.register();
        this.world = this.createWorld();
        network.network = this.network.createNetworkObject(host, port);
        this.network.onStartService();
        this.onStartService(this.network);
    }
    
    /**
     * Stops execution of the application.
     */
    public void stopNetworkService() {
        this.network.onStopService();
        this.guiFrame.stop();
        this.onStopService(network);
    }

    @Override
    public void simpleInitApp() {
        this.assets = new AssetsRegister(assetManager);
        this.registerAssets(assets);
        System.out.println("assets loaded");
        
        this.network = this.createNetworkHandler();
        
        guiFrame = new AppFrame(this.appName);
        registerScreens(guiFrame);

        this.input = new Console();
        this.input.add(new CommonConsole());
        this.setPauseOnLostFocus(false);
    }
    
    private void nextState() {
        if (this.currentState != null) {
            this.currentState.setEnabled(false);
        }
        this.currentState = stateManager.getState(this.nextState);
        this.currentState.setEnabled(true);
        this.nextState = null;
    }

    @Override
    public void update() {
        super.update();
        this.network.processNetworkQueue();
        this.input.readCommands(this);
        
        if (this.nextState != null) {
            this.nextState();
        }
    }
}
