package common.core;

import com.jme3.asset.AssetManager;
import com.jme3.asset.ModelKey;
import com.jme3.asset.TextureKey;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.font.BitmapFont;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import java.util.HashMap;

/**
 * Stores references to loaded assets. This class is used as part of the NetworkApp
 * to specify an entry point for loading all of the assets at a convenient time.
 * @author Dmitry Matveev
 */
public class AssetsRegister {
    
    private AssetManager am;
    private HashMap<String, TextureKey> textures;
    private HashMap<String, ModelKey> models;
    private HashMap<ColorRGBA, Material> materials;
    private HashMap<String, BitmapFont> fonts;
    
    /**
     * Creates new instance of the AssetsRegister.
     * @param am application assets manager instance
     */
    public AssetsRegister(AssetManager am) {
        this.am = am;
        this.am.registerLocator("./assets", FileLocator.class);
        this.models = new HashMap<String, ModelKey>();
        this.materials = new HashMap<ColorRGBA, Material>();
        this.textures = new HashMap<String, TextureKey>();
        this.fonts = new HashMap<String, BitmapFont>();
    }
    
    /**
     * Loads texture and stores it under specified key.
     * @param key key for the texture
     * @param texture path to the texture
     */
    public void registerTexture(String key, String texture) {
        TextureKey tkey = new TextureKey(texture);
        am.loadTexture(tkey);
        textures.put(key, tkey);
    }
    
    /**
     * Loads model and stores it under specified key.
     * @param key key for the model
     * @param model path to the model
     */
    public void registerModel(String key, String model) {
        ModelKey modelKey = new ModelKey(model);
        am.loadModel(model);
        models.put(key, modelKey);
    }
    
    /**
     * Loads material and stores it under specified key.
     * @param color key for the material
     * @param material path to the material
     */
    public void registerMaterial(ColorRGBA color, String material) {
        Material mat = new Material(am, material);
        mat.setTexture("ColorMap", getTexture("clear"));
        mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
        mat.setColor("Color", color);
        materials.put(color, mat);
    }
    
    /**
     * 
    /**
     * Loads font and stores it under specified key.
     * @param key key for the font
     * @param font path to the font
     */
    public void registerFont(String key, String font) {
        fonts.put(key, am.loadFont(font));
    }
    
    /**
     * Returns registered model.
     * @param key
     * @return 
     */
    public Spatial getModel(String key) {
        return am.loadModel(models.get(key));
    }
    
    /**
     * Returns registered texture.
     * @param name
     * @return 
     */
    public Texture getTexture(String name) {
        return am.loadTexture(textures.get(name));
    }
    
    /**
     * Returns registered material.
     * @param color
     * @return 
     */
    public Material getMaterial(ColorRGBA color) {
        return materials.get(color);
    }
    
    /**
     * Returns registered font.
     * @param name
     * @return 
     */
    public BitmapFont getFont(String name) {
        return fonts.get(name);
    }
}
