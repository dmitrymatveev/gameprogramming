package common.console;

import common.core.NetworkApp;

/**
 * Describes common commands for client and server.
 * 
 * @author Dmitry Matveev
 */
public class CommonConsole extends ConsoleCommander<NetworkApp> {

    @Override
    public <E extends Enum> Class<E> getCommandsEnum() {
        return (Class<E>) COMMAND.class;
    }
    
    public enum COMMAND implements Command<NetworkApp> {

        CLOSE("close", "<close> \t\t close network service") {
            @Override
            public void processCommand(NetworkApp app, String[] lines)
            throws ConsoleCommandException {
                app.stopNetworkService();
            }
        },
        EXIT("exit", "<exit> \t\t\t exit application") {
            @Override
            public void processCommand(NetworkApp app, String[] lines) 
            throws ConsoleCommandException {
                app.stopNetworkService();
                app.stop();
            }
        },
        HELP("help", "<help> [command name] \t display command info \n"
            + "<help> \t\t\t display all commands") {
            @Override
            public void processCommand(NetworkApp app, String[] lines)
            throws ConsoleCommandException {
                if (lines.length > 1) {
                    Command c = ConsoleCommander.find(app, lines[1]);
                    if (c != null) {
                        System.out.println(c.getDescription());
                    }
                } else {
                    for(Object c : app.getConsole().getAllCommands()) {
                        System.out.println(((Command) c).getDescription());
                    }
                }
            }
        };
        
        private String name;
        private String description;
        
        private COMMAND(String name, String desc) {
            this.name = name;
            this.description = desc;
        }

        @Override
        public String getDescription() {
            return this.description;
        }
        
        @Override
        public String getName() {
            return this.name;
        }
    }
}
