package common.console;

import common.console.ConsoleCommander.Command;
import common.console.ConsoleCommander.CommandComaparator;
import common.console.ConsoleCommander.ConsoleCommandException;
import common.core.NetworkApp;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * A collection of ConsoleCommander objects that provides entry point for the console
 * text input.
 * @author Dmitry Matveev
 */
public class Console<T extends NetworkApp> 
extends ArrayList<ConsoleCommander<T>> {

    private BufferedReader cin;

    /**
     * Creates new instance of the console input.
     */
    public Console() {
        this.init();
    }

    private void init() {
        try {
            this.cin = new BufferedReader(new InputStreamReader(System.in, java.nio.charset.Charset.defaultCharset().name()));
        } catch (final UnsupportedEncodingException e) {
            this.cin = new BufferedReader(new InputStreamReader(System.in));
        }
    }

    private void processCommand(T app, String[] lines) {
        for (ConsoleCommander p : this) {
            try {
                p.processCommand(app, lines);
            } catch (ConsoleCommandException ex) {
                ex.printMessage();
            }
        }
    }

    /**
     * Reads console input and sends it for parsing to registered console 
     * commanders.
     */
    public void readCommands(T app) {
        try {
            if (this.cin.ready()) {
                final String line = this.cin.readLine();
                final String[] lines = line.split(" ");
                this.processCommand(app, lines);
            }
        } catch (final IOException e) {
            Console.println(e.getCause().toString());
        }
    }

    /**
     * Returns all commands specified by all registered console commander
     * implementations.
     * @return list of all possible commands
     */
    public List<Command> getAllCommands() {
        ArrayList<Command> res = new ArrayList<Command>();
        Iterator<ConsoleCommander<T>> i = this.iterator();
        while (i.hasNext()) {
            ConsoleCommander<T> next = i.next();
            for (Enum c : next.getCommandsEnum().getEnumConstants()) {
                res.add((Command) c);
            }
        }
        Collections.sort(res, new CommandComaparator());
        return res;
    }
    
    /**
     * Helper method to write to console.
     * @param str 
     */
    public static void println(String str) {
        System.out.println(str);
    }
}
