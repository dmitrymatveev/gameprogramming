package common.console;

import common.console.ConsoleCommander.Command;
import common.core.NetworkApp;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Command parser which is a visitor to a class of type T. Implementing classes must 
 * provide an Enum structure that extends Command interface.
 * @author Dmitry Matveev
 * @param <T> class type this console parser is intended to visit
 */
public abstract class ConsoleCommander<T extends NetworkApp> {
    
    /**
     * Returns an Enum structure that describes all commands of the console commander
     * implementation.
     * @param <E> Enum
     * @return implementation based Enum of commands
     */
    public abstract <E extends Enum> Class<E> getCommandsEnum();
    
    private <E extends Enum> Command<T> findCommand(Class<E> enumData, String str) {
        Command<T> res = null;
        for(E val : enumData.getEnumConstants()) {
            if(val.toString().equalsIgnoreCase(str)) {
                res = (Command<T>) val;
            }
        }
        return res;
    }
    
    /**
     * Propagates parsing and execution of the command to console commander
     * implementations.
     * @param app object being controlled by this console commander
     * @param command console input
     * @throws common.console.ConsoleCommander.ConsoleCommandException 
     */
    public void processCommand(T app, String[] command)
    throws ConsoleCommandException {
        if (command.length > 0) {
            Command<T> c = this.findCommand(this.getCommandsEnum(), command[0]);
            if (c != null) {
                c.processCommand(app, command);
            }
        }
    }
    
    /**
     * Description of a command and an entry point for the execution of the
     * command routines. Type parameter must be the same as its encapsulating
     * console parser type.
     * @param <T> class type this command is intended to control
     */
    public interface Command<T extends NetworkApp> {

        public void processCommand(T app, String[] lines) throws ConsoleCommandException;
        public String getDescription();
        public String getName();
    }
    
    /**
     * Finds a command corresponding to a provided string.
     * @param <E> type of the command that is matched by the string.
     * @param app container of the ConsoleCommander implementation object
     * @param str string coded command
     * @return command or null if no matches were found
     */
    public static <E extends Command> E find(NetworkApp app, String str) {
        E res = null;
        Iterator it = app.getConsole().iterator();
        while(it.hasNext()) {
            ConsoleCommander next = (ConsoleCommander) it.next();
            for(Object c : next.getCommandsEnum().getEnumConstants()) {
                if (c.toString().equalsIgnoreCase(str)) {
                    res = (E) c;
                }
            }
        }
        return res;
    }
    
    /**
     * Used to sort command list.
     */
    protected static class CommandComaparator implements Comparator<Command> {

        @Override
        public int compare(Command o1, Command o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    }
    
    /**
     * Used to indicate that a command can crash an application.
     */
    public static class ConsoleCommandException extends Throwable {

        public ConsoleCommandException(String message) {
            super(message);
        }
        
        /**
         * Prints a message of this exception to the console.
         */
        public void printMessage() {
            System.out.println(this.getMessage());
        }
    }
}